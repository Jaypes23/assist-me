package androidstudio.assistme;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 11/23/2017.
 */

public class D_CardAdapter extends BaseAdapter {
    Context c;
    ArrayList<D_MenuClass> items;
    ArrayList<String> itemshldr = new ArrayList<>();
    ArrayList<String> totalhldr = new ArrayList<>();
    ArrayList<String> listitemshldr = new ArrayList<>();
    MaterialDialog inputQuan;
    D_CartListener mListener;
    String namehldr,quanhldr,pricehldr;
    int validationindex;
    int posindex;
    String validationstring = "false";
    String itemfromarray;

    public D_CardAdapter(Context c, ArrayList<D_MenuClass> items){
        this.c = c;
        this.items = items;
    }


    public void setListener(D_CartListener listener){
        mListener = listener;
    }

    public void removeItem(int position){
        posindex = position;
        mListener.RemovefromCart(validationindex);
        itemshldr.remove(validationindex);
        totalhldr.remove(validationindex);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.cardmodel, viewGroup, false);
        }

        final D_MenuClass m = (D_MenuClass) this.getItem(i);
        ImageView img = (ImageView) view.findViewById(R.id.itemimage);
        TextView tvname = (TextView) view.findViewById(R.id.itemname);
        TextView tvprice = (TextView) view.findViewById(R.id.itemprice);
        FancyButton btnAddCart = (FancyButton) view.findViewById(R.id.btnAddtoCart);

        img.setImageResource(m.getItemimage());
        tvname.setText(m.getItemname());
        tvprice.setText(m.getItemprice());

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
                View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                optionheader.setText("What would you like?");
                negative.setText("Per Case");
                positive.setText("Per Piece");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.hide();
                        OptionalertDialog.dismiss();
                        if(itemshldr.size() > 0){
                            for(int x=0;x<itemshldr.size();x++){
                                itemfromarray = itemshldr.get(x);
                                String parser = itemfromarray;
                                String delims = "[/]+";
                                String[] tokens = parser.split(delims);
                                if(m.getItemname().equals(String.valueOf(tokens[0]))){
                                    validationstring = "true";
                                    namehldr = String.valueOf(tokens[0]);
                                    pricehldr = String.valueOf(tokens[1]);
                                    quanhldr = String.valueOf(tokens[2]);
                                    validationindex = x;
                                }
                            }
                            if(validationstring.equals("true")){
                                inputQuan = new MaterialDialog.Builder(c)
                                        .title("Add to Cart")
                                        .content("Update Quantity")
                                        .inputType(InputType.TYPE_CLASS_NUMBER)
                                        .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                    Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                                }else{
                                                    if(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")){
                                                        mListener.RemovefromCart(validationindex);
                                                        itemshldr.remove(validationindex);
                                                        totalhldr.remove(validationindex);
                                                        Toast.makeText(c, m.getItemname() + " removed from cart" , Toast.LENGTH_SHORT).show();
                                                        validationstring = "false";
                                                        quanhldr = null;
                                                    }
                                                    else{
                                                        String parser = pricehldr;
                                                        String delims = "[₱]+";
                                                        String[] pricetokens = parser.split(delims);
                                                        Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                        Double total = (12.00 * Double.parseDouble(pricetokens[1])) * Double.parseDouble(String.valueOf(quaninput));
                                                        pricehldr = String.valueOf(Double.valueOf(pricehldr) * 12.00);
                                                        String itemtocart = "Case of "+namehldr+"/"+pricehldr+"/"+inputQuan.getInputEditText().getText().toString();
                                                        mListener.UpdateCart(validationindex,itemtocart, String.valueOf(total));
                                                        itemshldr.set(validationindex, "Case of "+namehldr+"/"+pricehldr+"/"+inputQuan.getInputEditText().getText().toString());
                                                        Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " Cases " + m.getItemname() + " item quantity updated" , Toast.LENGTH_SHORT).show();
                                                        validationstring = "false";
                                                        quanhldr = null;
                                                    }
                                                }
                                            }
                                        })
                                        .positiveText("Continue")
                                        .neutralText("Cancel")
                                        .cancelable(false)
                                        .show();
                            }
                            else{
                                quanhldr = "0";
                                inputQuan = new MaterialDialog.Builder(c)
                                        .title("Add to Cart")
                                        .content("Insert Quantity")
                                        .inputType(InputType.TYPE_CLASS_NUMBER)
                                        .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                    Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                                }
                                                else{
                                                    if(!(String.valueOf(inputQuan.getInputEditText().getText()).equals("0"))){
                                                        String parser = m.getItemprice();
                                                        String delims = "[₱]+";
                                                        String[] pricetokens = parser.split(delims);
                                                        Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                        Double total = (12.00 * Double.parseDouble(pricetokens[1])) * Double.parseDouble(String.valueOf(quaninput));
                                                        Double caseprice = Double.valueOf(pricetokens[1]) * 12.00;
                                                        Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                        String itemtocart = "Case of "+m.getItemname()+"/"+caseprice+"/"+inputQuan.getInputEditText().getText().toString();
                                                        mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                        itemshldr.add("Case of "+m.getItemname()+"/"+caseprice+"/"+inputQuan.getInputEditText().getText().toString());
                                                        totalhldr.add(String.valueOf(total));
                                                        Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " Cases of " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                                        })
                                        .positiveText("Continue")
                                        .neutralText("Cancel")
                                        .cancelable(false)
                                        .show();

                            }
                        }else{
                            quanhldr = "0";
                            inputQuan = new MaterialDialog.Builder(c)
                                    .title("Add to Cart")
                                    .content("Input Quantity")
                                    .inputType(InputType.TYPE_CLASS_NUMBER)
                                    .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                if(!(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")) || !(String.valueOf(inputQuan.getInputEditText().getText()).equals(""))){
                                                    String parser = m.getItemprice();
                                                    String delims = "[₱]+";
                                                    String[] pricetokens = parser.split(delims);
                                                    Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                    Double total = (12.00 * Double.parseDouble(pricetokens[1])) * Double.parseDouble(String.valueOf(quaninput));
                                                    Double caseprice = Double.valueOf(pricetokens[1]) * 12.00;
                                                    Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                    String itemtocart = "Case of "+m.getItemname()+"/"+caseprice+"/"+inputQuan.getInputEditText().getText().toString();
                                                    mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                    itemshldr.add("Case of "+m.getItemname()+"/"+caseprice+"/"+inputQuan.getInputEditText().getText().toString());
                                                    totalhldr.add(String.valueOf(total));
                                                    Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " Cases of " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    })
                                    .positiveText("Continue")
                                    .neutralText("Cancel")
                                    .cancelable(false)
                                    .show();
                        }
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.hide();
                        OptionalertDialog.dismiss();
                        if(itemshldr.size() > 0){
                            for(int x=0;x<itemshldr.size();x++){
                                itemfromarray = itemshldr.get(x);
                                String parser = itemfromarray;
                                String delims = "[/]+";
                                String[] tokens = parser.split(delims);
                                if(m.getItemname().equals(String.valueOf(tokens[0]))){
                                    validationstring = "true";
                                    namehldr = String.valueOf(tokens[0]);
                                    pricehldr = String.valueOf(tokens[1]);
                                    quanhldr = String.valueOf(tokens[2]);
                                    validationindex = x;
                                }
                            }
                            if(validationstring.equals("true")){
                                inputQuan = new MaterialDialog.Builder(c)
                                        .title("Add to Cart")
                                        .content("Update Quantity")
                                        .inputType(InputType.TYPE_CLASS_NUMBER)
                                        .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                    Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                                }else{
                                                    if(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")){
                                                        String parser = pricehldr;
                                                        String delims = "[₱]+";
                                                        String[] pricetokens = parser.split(delims);
                                                        Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                        Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                        mListener.RemovefromCart(validationindex);
                                                        itemshldr.remove(validationindex);
                                                        totalhldr.remove(validationindex);
                                                        Toast.makeText(c, m.getItemname() + " removed from cart" , Toast.LENGTH_SHORT).show();
                                                        validationstring = "false";
                                                        quanhldr = null;
                                                    }
                                                    else{
                                                        String parser = pricehldr;
                                                        String delims = "[₱]+";
                                                        String[] pricetokens = parser.split(delims);
                                                        Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                        Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                        String itemtocart = namehldr+"/"+pricehldr+"/"+inputQuan.getInputEditText().getText().toString();
                                                        mListener.UpdateCart(validationindex,itemtocart, String.valueOf(total));
                                                        itemshldr.set(validationindex, namehldr+"/"+pricehldr+"/"+inputQuan.getInputEditText().getText().toString());
                                                        Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " " + m.getItemname() + " item quantity updated" , Toast.LENGTH_SHORT).show();
                                                        validationstring = "false";
                                                        quanhldr = null;
                                                    }
                                                }
                                            }
                                        })
                                        .positiveText("Continue")
                                        .neutralText("Cancel")
                                        .cancelable(false)
                                        .show();
                            }
                            else{
                                quanhldr = "0";
                                inputQuan = new MaterialDialog.Builder(c)
                                        .title("Add to Cart")
                                        .content("Insert Quantity")
                                        .inputType(InputType.TYPE_CLASS_NUMBER)
                                        .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                    Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                                }
                                                else{
                                                    if(!(String.valueOf(inputQuan.getInputEditText().getText()).equals("0"))){
                                                        String parser = m.getItemprice();
                                                        String delims = "[₱]+";
                                                        String[] pricetokens = parser.split(delims);
                                                        Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                        Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                        String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+inputQuan.getInputEditText().getText().toString();
                                                        mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                        itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+inputQuan.getInputEditText().getText().toString());
                                                        totalhldr.add(String.valueOf(total));
                                                        Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                                        })
                                        .positiveText("Continue")
                                        .neutralText("Cancel")
                                        .cancelable(false)
                                        .show();

                            }
                        }else{
                            quanhldr = "0";
                            inputQuan = new MaterialDialog.Builder(c)
                                    .title("Add to Cart")
                                    .content("Input Quantity")
                                    .inputType(InputType.TYPE_CLASS_NUMBER)
                                    .input("Quantity", quanhldr, new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                                Toast.makeText(c, "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                if(!(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")) || !(String.valueOf(inputQuan.getInputEditText().getText()).equals(""))){
                                                    String parser = m.getItemprice();
                                                    String delims = "[₱]+";
                                                    String[] pricetokens = parser.split(delims);
                                                    Double quaninput = Double.parseDouble(String.valueOf(inputQuan.getInputEditText().getText()));
                                                    Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                    Toast.makeText(c, String.valueOf(total), Toast.LENGTH_SHORT).show();
                                                    String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+inputQuan.getInputEditText().getText().toString();
                                                    mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                    itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+inputQuan.getInputEditText().getText().toString());
                                                    totalhldr.add(String.valueOf(total));
                                                    Toast.makeText(c, inputQuan.getInputEditText().getText().toString() + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    })
                                    .positiveText("Continue")
                                    .neutralText("Cancel")
                                    .cancelable(false)
                                    .show();
                        }
                    }
                });
                OptionalertDialog.show();
            }

        });

        return view;
    }
}
