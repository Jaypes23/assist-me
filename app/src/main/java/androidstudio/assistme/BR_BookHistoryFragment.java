package androidstudio.assistme;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_BookHistoryFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    public String uid,orderid,orderdate,address,contact,orderservice, total, orderstatus, orderiderid;
    B_BookingClass lm;
    ArrayList<B_BookingClass> listmenu;
    ArrayList<String> bookdetails;
    B_HistoryAdapter itemAdapter;

    //UI
    @BindView(R.id.clienthistorylist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView header;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookriderhistory, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        lm = new B_BookingClass();
        listmenu = new ArrayList<>();
        bookdetails = new ArrayList<>();

        RetrieveBookingHistory();

        itemAdapter = new B_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_bookinginfo, null);
                dialogBuilder.setView(dialogView);
                String valhldr = bookdetails.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[/]+";
                String[] tokens = parser.split(delims);
                String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordservice = String.valueOf(tokens[5]);
                String ordstatus = String.valueOf(tokens[6]);

                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);

                tvordernum.setText("Booking ID: "+ordid);
                tvordtotal.setText("Booking Fee: "+ordtotal);
                tvordstatus.setText("Finished");
                tvordaddress.setText("Booking Address: "+ordadd);
                tvordcontact.setText("Contact Number: "+ordcon);
                tvordchange.setText("Service Requested: "+ordservice);
                tvordtimestamp.setText("Delivery Timestamp: "+orddate);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                // Cancel Button
                Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancellist);
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                    }
                });
                alertDialog.show();
            }});
    }

    public void RetrieveBookingHistory() {
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final B_BookingClass book = dataSnapshot.getValue(B_BookingClass.class);
                        uid = book.getUid();
                        orderid = book.getBookingid();
                        address = book.getBookingaddress();
                        contact = book.getBookingcontact();
                        total = book.getBookingtotal();
                        orderdate = book.getBookingtimestamp();
                        orderservice = book.getBookingservice();
                        orderstatus = book.getBookingstatus();
                        orderiderid = book.getRiderid();
                        if(mAuth.getCurrentUser().getUid().equals(orderiderid)){
                            lm = new B_BookingClass();
                            lm.setBookingid(orderid);
                            lm.setBookingtimestamp(orderdate);
                            lm.setBookingaddress(address);
                            lm.setBookingcontact(contact);
                            lm.setBookingtotal(total);
                            lm.setBookingservice(orderservice);
                            listmenu.add(lm);
                            itemAdapter.notifyDataSetChanged();
                            bookdetails.add(orderid+"/"+orderdate+"/"+address+"/"+contact+"/"+total+"/"+orderservice+"/"+orderstatus);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}

