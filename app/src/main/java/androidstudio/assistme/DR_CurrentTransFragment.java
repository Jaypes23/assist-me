package androidstudio.assistme;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/1/2017.
 */

public class DR_CurrentTransFragment extends android.support.v4.app.Fragment implements LocationListener {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, orderchangefor, ordertotal, orderlat, orderlong, ordernongeoadd;
    String firebaseorderstatus;
    ArrayList<String> orderitemsarray;
    String arrayitemshldr;
    Activity activity;
    public String getLocStat = "true";
    LocationManager locationManager;
    Handler handler;
    Location location;


    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delchangefor)
    TextView tvorderchangefor;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.deliveryitemlist)
    ListView lvitemlist;
    @BindView(R.id.buttongetdirections)
    Button btnGetDirections;
    @BindView(R.id.buttonfinishorder)
    Button btnFinish;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;

    public DR_CurrentTransFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_currentorder, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        RetrieveOrderData();
        getLocation();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordercontact, null)));
            }
        });

        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "waze://?" + orderlong + "," + orderlong + "&navigate=yes";
                startActivity(new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(uri)));
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                optionheader.setText("Finish Order?");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getLocStat = "false";
                        dialog = new MaterialDialog.Builder(getActivity())
                                .title("Delivery Successful!")
                                .content("You can now choose a new customer")
                                .positiveText("Close")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        mDatabase.child("Orders").child(orderid).child("orderstatus").setValue("delivered");
                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("ordertransaction", "false");
                                        editor.putString("orderid", "none");
                                        editor.putString("orderaddress", "none");
                                        editor.putString("ordercontact", "none");
                                        editor.putString("ordertimestamp", "none");
                                        editor.putString("orderchangefor", "none");
                                        editor.putString("ordertotal", "none");
                                        editor.putString("orderlatitude", "none");
                                        editor.putString("orderlongitude", "none");
                                        editor.putString("nongeolocadd", "none");
                                        orderitemsarray = new ArrayList<>();
                                        Set<String> set = new HashSet<String>();
                                        set.addAll(orderitemsarray);
                                        editor.putStringSet("orderitemarray", set);
                                        editor.apply();

                                        OptionalertDialog.dismiss();

                                        Fragment fragment = new DR_MainFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.contentContainer, fragment);
                                        fragmentTransaction.remove(new DR_CurrentTransFragment());
                                        fragmentTransaction.commit();
                                    }
                                })
                                .show();
                    }
                });
                OptionalertDialog.show();
            }
        });
    }

    public void RetrieveOrderData() {
        orderitemsarray = new ArrayList<>();
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("orderid", "ID");
        orderaddress = prefs.getString("orderaddress", "Address");
        ordercontact = prefs.getString("ordercontact", "Contact");
        ordertimestamp = prefs.getString("ordertimestamp", "Timestamp");
        orderchangefor = prefs.getString("orderchangefor", "ChangeFor");
        ordertotal = prefs.getString("ordertotal", "Total");
        orderlat = prefs.getString("orderlatitude", "Latitude");
        orderlong = prefs.getString("orderlongitude", "Longitude");
        ordernongeoadd = prefs.getString("nongeolocadd", "NonGeoAdd");
        Set<String> set = prefs.getStringSet("orderitemarray", null);
        orderitemsarray = new ArrayList<String>(set);

        if (orderaddress.equals(ordernongeoadd)) {
            tvorderaddress.setText("Customer Delivery Address: " + orderaddress);
        } else {
            tvorderaddress.setText("Customer Delivery Address: " + orderaddress
                    + "\n" + "Customer Non Google Maps Address: " + ordernongeoadd);
        }

        tvorderid.setText("Current Order ID: " + orderid);
        tvordercontact.setText("Customer Contact Number: " + ordercontact);
        tvordertimestamp.setText("Delivery Timestamp: " + ordertimestamp);
        tvordertotal.setText("Order Total: " + ordertotal);
        tvorderchangefor.setText("Requested Change For: " + orderchangefor);
        tvorderstatus.setText("Delivery not yet finished!");

        lvitemlist.setAdapter(new D_ListAdapter(getActivity(), ViewCartList()));
        lvitemlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int itemPosition = i;
                String  itemValue = (String) lvitemlist.getItemAtPosition(i);
                Toast.makeText(getActivity(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private ArrayList ViewCartList(){
        ArrayList<D_MenuClass> listmenu = new ArrayList<>();
        D_MenuClass lm = new D_MenuClass();
        for(int i = 0; i<orderitemsarray.size();i++){
            lm = new D_MenuClass();
            arrayitemshldr = orderitemsarray.get(i);
            String parser = arrayitemshldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else {
                Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocationGPS != null) {
                    return lastKnownLocationGPS;
                } else {
                    Location loc =  locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    System.out.println("1::"+loc);
                    System.out.println("2::"+loc.getLatitude());
                    System.out.println("3::"+loc.getLongitude());

                    String latitude = String.valueOf(loc.getLatitude());
                    String longitude = String.valueOf(loc.getLongitude());

                    mDatabase.child("Orders").child(orderid).child("Rider").child("driverlat").setValue(latitude);
                    mDatabase.child("Orders").child(orderid).child("Rider").child("driverlong").setValue(longitude);

                    return loc;
                }
            }
        } else {
            return null;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
