package androidstudio.assistme;

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/1/2017.
 */

public class DR_MainFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    String USER_TYPE;
    D_OrderClass lm;
    ArrayList<D_OrderClass> listmenu;
    ArrayList<String> orderdetails;
    ArrayList<ArrayList<String>> itemsfromhistoryarray;
    ArrayList<String> itemhldr;
    public String isEmpty = "true";
    public String uid,orderid,orderdate,address,contact,total,changefor,latitude,longitude,nongeoloc,status;
    public String driverlat, driverlong;
    public ArrayList<String> items;
    public String prefuid, prefemail, prefname, prefcontact, prefaddress, prefordertrans;
    LocationManager mLocationManager;
    Location myLocation;

    //UI
    @BindView(R.id.orderlist)
    ListView orderlist;

    D_HistoryAdapter itemAdapter;

    MaterialDialog dialog;

    public DR_MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clientorders, container, false);
        ButterKnife.bind(this, view);
        RetrieveUserData();
        Initialize();
        return view;
    }

    public void Initialize(){
        if(prefordertrans.equals("true")){
            Fragment fragment = new DR_CurrentTransFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.contentContainer, fragment);
            fragmentTransaction.remove(new DR_MainFragment());
            fragmentTransaction.commit();
        }
        else{
            dialog = new MaterialDialog.Builder(getActivity())
                    .title("Loading Orders")
                    .content("Please wait...")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            lm = new D_OrderClass();
            listmenu = new ArrayList<>();
            orderdetails = new ArrayList<>();
            itemsfromhistoryarray = new ArrayList<>();
            itemhldr = new ArrayList<>();
            items = new ArrayList<>();
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            itemAdapter = new D_HistoryAdapter(getActivity(), listmenu);
            orderlist.setAdapter(itemAdapter);
            RealtimeOrderHistory();

            orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                      AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                      LayoutInflater inflater = getActivity().getLayoutInflater();
                      View dialogView = inflater.inflate(R.layout.popup_orderclient, null);
                      dialogBuilder.setView(dialogView);
                      String valhldr = orderdetails.get(position);
                      itemhldr = itemsfromhistoryarray.get(position);
                      String stringtoparse = valhldr;
                      String parser = stringtoparse;
                      String delims = "[/]+";
                      String[] tokens = parser.split(delims);
                      final String ordid = String.valueOf(tokens[0]);
                      final String orddate = String.valueOf(tokens[1]);
                      final String ordadd = String.valueOf(tokens[2]);
                      final String ordcon = String.valueOf(tokens[3]);
                      final String ordtotal = String.valueOf(tokens[4]);
                      final String ordchangefor = String.valueOf(tokens[5]);
                      final String ordlat = String.valueOf(tokens[6]);
                      final String ordlong = String.valueOf(tokens[7]);
                      final String ordnongeo = String.valueOf(tokens[8]);

                      final ListView listView = (ListView) dialogView.findViewById(R.id.deliveryitemlist);
                      final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                      final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                      final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                      final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                      final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                      final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                      final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);

                      tvordernum.setText("Order ID: "+ordid);
                      tvordtotal.setText("Order Total: "+ordtotal);
                      tvordstatus.setText("Not Yet Delivered");
                      tvordaddress.setText("Delivery Address: "+ordadd);
                      tvordcontact.setText("Contact Number: "+ordcon);
                      tvordchange.setText("Requested Change For: "+ordchangefor);
                      tvordtimestamp.setText("Delivery Timestamp: "+orddate);

                      listView.setAdapter(new D_ListAdapter(getActivity(), ViewCartList()));
                      final AlertDialog alertDialog = dialogBuilder.create();
                      Window window = alertDialog.getWindow();
                      window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                      window.setGravity(Gravity.CENTER);

                      Button cancel_btn = (Button) dialogView.findViewById(R.id.buttonclose);
                      Button accept_btn = (Button) dialogView.findViewById(R.id.buttonaccept);

                      cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                   alertDialog.dismiss();
                                    }
                      });

                      accept_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                      AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                      LayoutInflater inflater = getActivity().getLayoutInflater();
                      View dialogView = inflater.inflate(R.layout.popup_choice, null);
                      dialogBuilder.setView(dialogView);

                      final AlertDialog OptionalertDialog = dialogBuilder.create();
                      Window window = OptionalertDialog.getWindow();
                      window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                      window.setGravity(Gravity.CENTER);

                      final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                      final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                      final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                      optionheader.setText("Accept Order?");

                      negative.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                            OptionalertDialog.dismiss();
                         }
                                                 });

                      positive.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                  getLastKnownLocation();
                                                  D_OrderClass order = new D_OrderClass(driverlat, driverlong, prefname);
                                                  mDatabase.child("Orders").child(ordid).child("Rider").setValue(order);
                                                  mDatabase.child("Orders").child(ordid).child("orderstatus").setValue("otw");

                                                  SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                       editor.putString("ordertransaction", "true");
                                                       editor.putString("orderid", ordid);
                                                       editor.putString("orderaddress", ordadd);
                                                       editor.putString("ordercontact", ordcon);
                                                       editor.putString("ordertimestamp", orddate);
                                                       editor.putString("orderchangefor", ordchangefor);
                                                       editor.putString("ordertotal", ordtotal);
                                                       editor.putString("orderlatitude", ordlat);
                                                       editor.putString("orderlongitude", ordlong);
                                                       editor.putString("nongeolocadd", ordnongeo);

                                                       Set<String> set = new HashSet<String>();
                                                       set.addAll(itemhldr);
                                                       editor.putStringSet("orderitemarray", set);
                                                       editor.apply();

                                                       OptionalertDialog.dismiss();

                                                       Fragment fragment = new DR_CurrentTransFragment();
                                                       FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                       FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                       fragmentTransaction.replace(R.id.contentContainer, fragment);
                                                       fragmentTransaction.remove(new DR_MainFragment());
                                                       fragmentTransaction.commit();
                                                     }
                                                  });
                                                  OptionalertDialog.show();
                                            }});
                                            alertDialog.show();
                                         }});
                       dialog.dismiss();
            }
      }

    public void RealtimeOrderHistory(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final D_OrderClass order = dataSnapshot.getValue(D_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        status = order.getOrderstatus();
                        latitude = order.getLatitude();
                        longitude = order.getLongitude();
                        nongeoloc = order.getRealaddress();
                        items = order.getItems();
                        if (status.equals("pending")) {
                            lm = new D_OrderClass();
                            isEmpty = "false";
                            lm.setOrdernum(orderid);
                            lm.setTimestamp(orderdate);
                            lm.setAddress(address);
                            lm.setContactnum(contact);
                            lm.setCarttotal(total);
                            lm.setChangefor(changefor);
                            lm.setItems(items);
                            lm.setLatitude(latitude);
                            lm.setLongitude(longitude);
                            lm.setRealaddress(nongeoloc);
                            listmenu.add(lm);
                            orderdetails.add(orderid + "/" + orderdate + "/" + address + "/" + contact + "/" + total + "/" + changefor + "/" + latitude + "/" + longitude + "/" + nongeoloc);
                            itemsfromhistoryarray.add(items);
                            itemAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final D_OrderClass order = dataSnapshot.getValue(D_OrderClass.class);
                        String changeorderid = order.getOrdernum();
                        String changeorderstatus = order.getOrderstatus();
                        if (changeorderstatus.equals("otw") || changeorderstatus.equals("delivered")) {
                            isEmpty = "false";
                            for(int val=0; val<listmenu.size();val++){
                                String changevalhldr = orderdetails.get(val);
                                String changestringtoparse = changevalhldr;
                                String changeparser = changestringtoparse;
                                String changedelims = "[/]+";
                                String[] changetokens = changeparser.split(changedelims);
                                final String ordid = String.valueOf(changetokens[0]);
                                if(ordid.equals(changeorderid)){
                                    listmenu.remove(val);
                                    orderdetails.remove(val);
                                    itemsfromhistoryarray.remove(val);
                                    itemAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

        private ArrayList ViewCartList(){
        ArrayList<D_MenuClass> listmenu = new ArrayList<>();
        D_MenuClass lm = new D_MenuClass();
        for(int i = 0; i<itemhldr.size();i++){
            lm = new D_MenuClass();
            String arrayvalhldr = itemhldr.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    public void RetrieveUserData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        prefordertrans = prefs.getString("ordertransaction", "order");
        prefuid = prefs.getString("userid", "UID");
        prefname = prefs.getString("username", "Name");
        prefemail = prefs.getString("useremail", "Email");
        prefaddress = prefs.getString("useraddress", "Address");
        prefcontact = prefs.getString("usercontact", "Contact");
    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else {
                Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocationGPS != null) {
                    return lastKnownLocationGPS;
                } else {
                    Location loc =  locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    System.out.println("1::"+loc);
                    System.out.println("2::"+loc.getLatitude());
                    System.out.println("3::"+loc.getLongitude());

                    driverlat = String.valueOf(loc.getLatitude());
                    driverlong = String.valueOf(loc.getLongitude());
                    return loc;
                }
            }
        } else {
            return null;
        }
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    driverlat = String.valueOf(bestLocation.getLatitude());
                    driverlong = String.valueOf(bestLocation.getLongitude());
                }
            }

        }
        return bestLocation;
    }
}
