package androidstudio.assistme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_CurrentTransFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, ordername, orderaddress, ordercontact, ordertimestamp, ordertotal, orderlat, orderlong, orderstat, orderservice;
    String firebaseorderstatus;
    Activity activity;

    //Location Variables
    String faddress;
    LocationManager mLocationManager;
    Location myLocation;
    Geocoder geocoder;
    List<Address> addresses;
    String latitude, longitude;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.delcustname)
    TextView tvordername;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delservice)
    TextView tvorderservice;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.buttongetdirections)
    Button btnGetDirections;
    @BindView(R.id.buttonfinishorder)
    Button btnFinish;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookridercurrent, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        RetrieveBookingData();
        getLastKnownLocation();
        onChangeLocation();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordercontact, null)));
            }
        });

        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String uri = "waze://?" + orderlong + "," + orderlong + "&navigate=yes";
//                startActivity(new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse(uri)));
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+orderlat+","+orderlong));
                startActivity(intent);

            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                optionheader.setText("Finish Booking?");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog = new MaterialDialog.Builder(getActivity())
                                .title("Successful!")
                                .content("You can now choose a new customer")
                                .positiveText("Close")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        OptionalertDialog.dismiss();
                                        mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("delivered");
                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("booktransaction", "false");
                                        editor.putString("bookid", "none");
                                        editor.putString("bookname", "none");
                                        editor.putString("bookaddress", "none");
                                        editor.putString("bookcontact", "none");
                                        editor.putString("booktimestamp", "none");
                                        editor.putString("booktotal", "none");
                                        editor.putString("booklatitude", "none");
                                        editor.putString("booklongitude", "none");
                                        editor.putString("bookservice", "none");
                                        editor.apply();

                                        dialog.dismiss();

                                        Fragment fragment = new BR_MainFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.remove(new BR_CurrentTransFragment());
                                        fragmentTransaction.replace(R.id.contentContainer, fragment);
                                        fragmentTransaction.commit();
                                    }
                                })
                                .show();
                    }
                });
                OptionalertDialog.show();
            }
        });
    }


    public void RetrieveBookingData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("bookid", "ID");
        ordername = prefs.getString("bookname", "Name");
        orderaddress = prefs.getString("bookaddress", "Address");
        ordercontact = prefs.getString("bookcontact", "Contact");
        ordertimestamp = prefs.getString("booktimestamp", "Timestamp");
        orderservice = prefs.getString("bookservice", "Service");
        ordertotal = prefs.getString("booktotal", "Total");
        orderlat = prefs.getString("booklatitude", "Latitude");
        orderlong = prefs.getString("booklongitude", "Longitude");
        orderstat = prefs.getString("bookstatus", "Status");

        tvorderid.setText("Booking ID: " + orderid);
        tvordername.setText("Customer Name:" + ordername);
        tvorderaddress.setText("Customer Address: " + orderaddress);
        tvordertimestamp.setText("Booking Timestamp: " + ordertimestamp);
        tvordertotal.setText("Service Fee: " + ordertotal);
        tvorderstatus.setText("Service not yet finished");
        tvorderservice.setText("The Service requested: " + orderservice);
    }


    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    latitude  = String.valueOf(bestLocation.getLatitude());
                    longitude = String.valueOf(bestLocation.getLongitude());

                    mDatabase.child("Bookings").child(orderid).child("driverlat").setValue(latitude);
                    mDatabase.child("Bookings").child(orderid).child("driverlong").setValue(longitude);
                }
            }

        }
        return bestLocation;
    }

    public void onChangeLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        {
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    getLastKnownLocation();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {}

                @Override
                public void onProviderEnabled(String s) {}

                @Override
                public void onProviderDisabled(String s) {}
            };
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);
        }
    }
}
