package androidstudio.assistme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/4/2017.
 */

public class BU_MainFragment extends android.support.v4.app.Fragment {

    //UI
    @BindView(R.id.checkout_address)
    EditText etAddress;
    @BindView(R.id.checkout_contact)
    EditText etContact;
    @BindView(R.id.servicespinner)
    MaterialSpinner msspinner;
    @BindView(R.id.servicetotal)
    TextView tvtotal;
    @BindView(R.id.buttonfind)
    Button btnBook;
    MaterialDialog dialog;

    //Preference
    private String USER_TYPE;
    String ordertransaction, uid, name, email, address, contact;
    String ordernum;
    String reportDate;
    String ordertotal;
    String status;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Location Variables
    String faddress;
    LocationManager mLocationManager;
    Location myLocation;
    Geocoder geocoder;
    List<Address> addresses;
    String latitude, longitude;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookservice, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        RetrieveUserData();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        etAddress.setText(address);
        etContact.setText(contact);

        msspinner.setItems("Select Service","Electrical Engineer", "Plumber", "Construction");
        msspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                msspinner.setText(item);
                if(item.equals("Electrical Engineer")){
                    tvtotal.setVisibility(View.VISIBLE);
                    ordertotal = "250.00";
                    tvtotal.setText("Service Fee: "+ordertotal);
                }
                if(item.equals("Plumber")){
                    tvtotal.setVisibility(View.VISIBLE);
                    ordertotal = "150.00";
                    tvtotal.setText("Service Fee: "+ordertotal);
                }
                if(item.equals("Construction")){
                    tvtotal.setVisibility(View.VISIBLE);
                    ordertotal = "450.00";
                    tvtotal.setText("Service Fee: "+ordertotal);
                }
            }
        });

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if(msspinner.getText().equals("Select Service")){
                    Toast.makeText(getActivity(), "Please select a Service first!", Toast.LENGTH_SHORT).show();
                }
                else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.popup_choice, null);
                    dialogBuilder.setView(dialogView);

                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                    Window window = OptionalertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);

                    final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                    final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                    final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                    optionheader.setText("Book Now?");

                    negative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.dismiss();
                        }
                    });

                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.dismiss();
                            getLastKnownLocation();
                            Date currentTime = Calendar.getInstance().getTime();
                            DateFormat df = new SimpleDateFormat("MM-dd-yyyy-HH:mm:ss");
                            reportDate = df.format(currentTime);
                            final int random = new Random().nextInt(991) + 10;
                            ordernum = "SER-" + uid.substring(20) + random;

                            B_BookingClass book = new B_BookingClass(mAuth.getCurrentUser().getUid(),ordernum,name,etAddress.getText().toString(),etContact.getText().toString(),
                                    msspinner.getText().toString(),"pending",reportDate, latitude, longitude, ordertotal, "none", "NR Yet", "NR Yet");
                            mDatabase.child("Bookings").child(ordernum).setValue(book);

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(),R.style.Dialog);
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.popup_findingmatch, null);
                            dialogBuilder.setView(dialogView);

                            final AlertDialog matchDialog = dialogBuilder.create();
                            Window window = matchDialog.getWindow();
                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            window.setGravity(Gravity.CENTER);

                            final PulsatorLayout pulsator = (PulsatorLayout) dialogView.findViewById(R.id.pulsator);
                            pulsator.start();

                            final FancyButton btnMatching = (FancyButton) dialogView.findViewById(R.id.btnMatching);
                            btnMatching.setText("Cancel Booking");

                            btnMatching.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    matchDialog.dismiss();
                                    mDatabase.child("Bookings").child(ordernum).removeValue();
                                    dialog = new MaterialDialog.Builder(getActivity())
                                            .title("Booking Cancelled!")
                                            .content("You have cancelled your booking")
                                            .positiveText("Close")
                                            .show();
                                }
                            });

                            matchDialog.show();

                            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
                            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                            mRootRef.child("Bookings")
                                    .child(ordernum)
                                    .addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {}

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                            if(String.valueOf(dataSnapshot.getKey()).equals("bookingstatus")){
                                                status = String.valueOf(dataSnapshot.getValue());
                                                if(status.equals("otw")){
                                                    matchDialog.dismiss();
                                                    btnMatching.setEnabled(false);
                                                    btnMatching.setText("Finding Finished");
                                                    dialog = new MaterialDialog.Builder(getActivity())
                                                            .title("Booking Update!")
                                                            .content("We have found you a rider!")
                                                            .positiveText("Proceed")
                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                @Override
                                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                                    editor.putString("booktransaction", "true");
                                                                    editor.putString("bookid", ordernum);
                                                                    editor.putString("bookcontact", etContact.getText().toString());
                                                                    editor.putString("bookaddress", etAddress.getText().toString());
                                                                    editor.putString("booktimestamp", reportDate);
                                                                    editor.putString("booklatitude", latitude);
                                                                    editor.putString("booklongitude", longitude);
                                                                    editor.putString("bookservice", msspinner.getText().toString());
                                                                    editor.putString("booktotal", ordertotal);
                                                                    editor.putString("bookstatus", "matched");
                                                                    editor.apply();

                                                                    Fragment fragment = new BU_CurrentTransFragment();
                                                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                                                    fragmentTransaction.remove(new BU_MainFragment());
                                                                    fragmentTransaction.commit();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            matchDialog.dismiss();
                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }
                    });
                    OptionalertDialog.show();
                }
            }
        });
    }

    public void RetrieveUserData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
        uid = prefs.getString("userid", "UID");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    latitude = String.valueOf(bestLocation.getLatitude());
                    longitude = String.valueOf(bestLocation.getLongitude());

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                        faddress = address;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return bestLocation;
    }


}
