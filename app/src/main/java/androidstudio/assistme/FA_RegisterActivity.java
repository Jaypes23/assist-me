package androidstudio.assistme;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 9/22/2017.
 */

public class FA_RegisterActivity extends AppCompatActivity {

    //EditText
    @BindView(R.id.reg_name) EditText etUserId;
    @BindView(R.id.reg_bday) EditText etBirthday;
    @BindView(R.id.reg_address) EditText etAddress;
    @BindView(R.id.reg_contact) EditText etContact;
    @BindView(R.id.reg_email) EditText etEmail;
    @BindView(R.id.reg_password) EditText etPassword;

    @BindView(R.id.reg_tlbday)
    TextInputLayout tlBday;

    //Button
    @BindView(R.id.btnBack)
    FancyButton btnBack;
    @BindView(R.id.btnRegister)
    FancyButton btnRegister;

    //Material Dialog
    MaterialDialog registerdialog;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    public int year,month,day,age;
    public String timestamp;
    public SimpleDateFormat currentTimeStamp;
    public String intentname, intentemail, intentuid;
    public String intentstatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        final Intent intent = getIntent();
        intentuid = intent.getStringExtra("user_id");
        intentname = intent.getStringExtra("user_name");
        intentemail = intent.getStringExtra("user_email");

        if(intentuid.equals("default")){
            intentstatus = "false";
        }else{
            intentstatus = "true";
        }

        if(intentstatus.equals("true")){
            etUserId.setText(intentname);
            etEmail.setText(intentemail);
            etUserId.setEnabled(false);
            etEmail.setEnabled(false);
        }


        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        timestamp = currentTimeStamp.format(new Date());

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(FA_RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        tlBday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(FA_RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FA_RegisterActivity.this, FA_LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerdialog = new MaterialDialog.Builder(FA_RegisterActivity.this)
                        .title("Signing Up")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();

                final String email = etEmail.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();
                final String name = etUserId.getText().toString().trim();
                final String birthday = etBirthday.getText().toString().trim();
                final String address = etAddress.getText().toString().trim();
                final String contactnum = etContact.getText().toString().trim();
                getAge(etBirthday);

                if (TextUtils.isEmpty(etUserId.getText().toString())) {
                    registerdialog.dismiss();
                    etUserId.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    registerdialog.dismiss();
                    etBirthday.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    registerdialog.dismiss();
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    registerdialog.dismiss();
                    etContact.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    registerdialog.dismiss();
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    registerdialog.dismiss();
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if(age < 18){
                    registerdialog.dismiss();
                    new MaterialDialog.Builder(FA_RegisterActivity.this)
                            .title("Register Failed")
                            .content("You must be 18 or older to use the App!")
                            .positiveText("Close")
                            .show();
                }
                else {
                    if(intentstatus.equals("false")){
                        mAuth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(FA_RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            FA_UserClass user = new FA_UserClass(name, address, contactnum, birthday, String.valueOf(age), email, password, "user");
                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                            registerdialog.dismiss();
                                            Toast.makeText(FA_RegisterActivity.this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(FA_RegisterActivity.this, FA_LoginActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            try {
                                                throw task.getException();
                                            } catch (FirebaseAuthUserCollisionException e) {
                                                new MaterialDialog.Builder(FA_RegisterActivity.this)
                                                        .title("Register Failed")
                                                        .content("Email already in use!")
                                                        .positiveText("Close")
                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                Intent intent = new Intent(FA_RegisterActivity.this, FA_RegisterActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                            }
                                                        })
                                                        .show();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                    }
                    else{
                        FA_UserClass user = new FA_UserClass(name, address, birthday, String.valueOf(age), contactnum, email, password, "user");
                        mDatabase.child("Users").child(intentuid).setValue(user);
                        registerdialog.dismiss();
                        Toast.makeText(FA_RegisterActivity.this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(FA_RegisterActivity.this, FA_LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public int getAge(EditText etBirthday){
        String parser = etBirthday.getText().toString();
        String delims = "[/]+";
        String[] tokens = parser.split(delims);

        int years = Integer.parseInt(tokens[2]);
        int yearCalculate = Calendar.getInstance().get(Calendar.YEAR);
        age = yearCalculate - years;
        return yearCalculate - years;
    }
    private static boolean editTextIsEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

}
