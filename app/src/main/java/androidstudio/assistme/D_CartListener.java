package androidstudio.assistme;

/**
 * Created by JP on 11/23/2017.
 */

public interface D_CartListener {
    //Cart Manipulation
    void AddtoCart(String itemcart, String total);
    void UpdateCart(int index, String itemcart, String total);
    void RemovefromCart(int index);
}
