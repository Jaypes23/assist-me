package androidstudio.assistme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 12/1/2017.
 */

public class FA_MainClientActivity extends AppCompatActivity {

    //Variables
    String ordertransaction,name,email,address,contact;
    android.support.v4.app.FragmentTransaction transaction;

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //Preference
    private String USER_TYPE;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityclient_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Assist ME");
        RetrieveUserData();
        Initialize();
    }

    public void Initialize(){
        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if(tabId == R.id.tab_orders){
                    if(ordertransaction.equals("false")){
                        toolbar.setTitle("Assist ME");
                        Fragment fragmentsreport = new BR_MainFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        transaction.replace(R.id.contentContainer, fragmentsreport);
                        transaction.commit();
                    }
                    else if(ordertransaction.equals("true")){
                        toolbar.setTitle("On the way");
                        Fragment fragmentsreport = new BR_CurrentTransFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        transaction.replace(R.id.contentContainer, fragmentsreport);
                        transaction.commit();
                    }
                }
                if (tabId == R.id.tab_history) {
                    Fragment fragmentsmap = new BR_BookHistoryFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.replace(R.id.contentContainer, fragmentsmap);
                    transaction.commit();
                }
                if(tabId == R.id.tab_profile){
                    Fragment fragmentsreport = new FA_ProfileFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.replace(R.id.contentContainer, fragmentsreport);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final FA_FirebaseClass FAFirebaseClass = new FA_FirebaseClass(FA_MainClientActivity.this);
            FAFirebaseClass.getUserInstance().signOut();
            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
            editor.remove("status");
            editor.remove("timehldr");
            editor.remove("username");
            editor.remove("useremail");
            editor.remove("useraddress");
            editor.remove("usercontact");
            editor.commit();
            Intent intent = new Intent(FA_MainClientActivity.this, FA_LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void RetrieveUserData(){
        SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

}
