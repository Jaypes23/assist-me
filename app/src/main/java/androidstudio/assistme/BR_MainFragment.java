package androidstudio.assistme;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_MainFragment extends android.support.v4.app.Fragment  {

    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.pulsator)
    PulsatorLayout pulsator;
    @BindView(R.id.btnMatching)
    FancyButton fbsearch;

    String bookid, bookaddress, bookcontact, bookcustname, bookservice, bookstatus, booktotal, booklat, booklong, bookttimestamp;
    String validationforrepeation = "false";
    String USER_TYPE;
    String drivername, drivercontact;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookrider, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        RetrieveDriverData();
        fbsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pulsator.start();
                tvheader.setText("Searching...");
                fbsearch.setText("Cancel");
                FindCustomer();

            }
        });
    }

    public void FindCustomer(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if (validationforrepeation.equals("false")) {
                            final B_BookingClass book = dataSnapshot.getValue(B_BookingClass.class);
                            bookcustname = book.getBookingcustname();
                            bookid = book.getBookingid();
                            bookaddress = book.getBookingaddress();
                            bookcontact = book.getBookingcontact();
                            bookservice = book.getBookingservice();
                            bookstatus = book.getBookingstatus();
                            booktotal = book.getBookingtotal();
                            bookttimestamp = book.getBookingtimestamp();
                            booklat = book.getBookinglat();
                            booklong = book.getBookinglong();
                            if (bookstatus.equals("pending")) {
                                validationforrepeation = "true";
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.popup_bookfoundrider, null);
                                dialogBuilder.setView(dialogView);

                                final AlertDialog OptionalertDialog = dialogBuilder.create();
                                Window window = OptionalertDialog.getWindow();
                                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                window.setGravity(Gravity.CENTER);

                                final TextView tvbookid = (TextView) dialogView.findViewById(R.id.delordernum);
                                final TextView tvstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                                final TextView tvname = (TextView) dialogView.findViewById(R.id.delcustname);
                                final TextView tvservice = (TextView) dialogView.findViewById(R.id.delservice);
                                final TextView tvaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                                final TextView tvcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                                final TextView tvtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                                final TextView tvtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                                final Button btncancel = (Button) dialogView.findViewById(R.id.buttonclose);
                                final Button btnaccept = (Button) dialogView.findViewById(R.id.buttonaccept);

                                tvbookid.setText(bookid);
                                tvstatus.setText("Status: Pending");
                                tvname.setText("Customer Name: " + bookcustname);
                                tvservice.setText("Chosen Service: " + bookservice);
                                tvaddress.setText("Address: " + bookaddress);
                                tvcontact.setText("Contact #: " + bookcontact);
                                tvtimestamp.setText("Booking Timestamp: " + bookttimestamp);
                                tvtotal.setText("Service Fee: " + booktotal);

                                btncancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        OptionalertDialog.dismiss();
                                        validationforrepeation = "false";
                                        tvheader.setText("Welcome");
                                        fbsearch.setText("Find Customer");
                                        pulsator.stop();
                                    }
                                });

                                btnaccept.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        OptionalertDialog.dismiss();
                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("booktransaction", "true");
                                        editor.putString("bookid", bookid);
                                        editor.putString("bookcustname", bookcustname);
                                        editor.putString("bookcontact", bookcontact);
                                        editor.putString("bookaddress", bookaddress);
                                        editor.putString("booktimestamp", bookttimestamp);
                                        editor.putString("booklatitude", booklat);
                                        editor.putString("booklongitude", booklong);
                                        editor.putString("bookservice", bookservice);
                                        editor.putString("booktotal", booktotal);
                                        editor.putString("bookstatus", "matched");
                                        editor.apply();

                                        mDatabase.child("Bookings").child(bookid).child("riderid").setValue(mAuth.getCurrentUser().getUid());
                                        mDatabase.child("Bookings").child(bookid).child("ridername").setValue(drivername);
                                        mDatabase.child("Bookings").child(bookid).child("ridercontact").setValue(drivercontact);
                                        mDatabase.child("Bookings").child(bookid).child("bookingstatus").setValue("otw");

                                        Fragment fragment = new BR_CurrentTransFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.contentContainer, fragment);
                                        fragmentTransaction.remove(new BR_MainFragment());
                                        fragmentTransaction.commit();
                                    }
                                });
                                OptionalertDialog.show();
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        fbsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationforrepeation = "false";
                tvheader.setText("Welcome");
                fbsearch.setText("Find Customer");
                pulsator.stop();
            }
        });
    }

    public void RetrieveDriverData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        drivername = prefs.getString("username", "Name");
        drivercontact = prefs.getString("usercontact", "Contact");
    }
}
