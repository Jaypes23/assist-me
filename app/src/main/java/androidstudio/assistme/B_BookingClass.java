package androidstudio.assistme;

/**
 * Created by JP on 12/4/2017.
 */

public class B_BookingClass {
    public String uid;
    public String bookingid;
    public String bookingcustname;
    public String bookingaddress;
    public String bookingcontact;
    public String bookingservice;
    public String bookingstatus;
    public String bookinglat;
    public String bookinglong;
    public String bookingtimestamp;
    public String bookingtotal;

    public String riderid;
    public String ridername;
    public String ridercontact;
    public String riderlat;
    public String riderlong;

    public B_BookingClass(){

    }

    public B_BookingClass(String uid, String bookingid, String bookingcustname, String bookingaddress, String bookingcontact,
                          String bookingservice, String bookingstatus, String bookingtimestamp,
                          String bookinglat, String bookinglong, String bookingtotal, String riderid, String ridername, String ridercontact) {
        this.uid = uid;
        this.bookingid = bookingid;
        this.bookingcustname = bookingcustname;
        this.bookingaddress = bookingaddress;
        this.bookingcontact = bookingcontact;
        this.bookingservice = bookingservice;
        this.bookingstatus = bookingstatus;
        this.bookingtimestamp = bookingtimestamp;
        this.bookinglat = bookinglat;
        this.bookinglong = bookinglong;
        this.bookingtotal = bookingtotal;
        this.riderid = riderid;
        this.ridername = ridername;
        this.ridercontact = ridercontact;
    }

    public String getBookingid() {
        return bookingid;
    }

    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    public String getBookingcustname() {
        return bookingcustname;
    }

    public void setBookingcustname(String bookingcustname) {
        this.bookingcustname = bookingcustname;
    }

    public String getBookingaddress() {
        return bookingaddress;
    }

    public void setBookingaddress(String bookingaddress) {
        this.bookingaddress = bookingaddress;
    }

    public String getBookingcontact() {
        return bookingcontact;
    }

    public void setBookingcontact(String bookingcontact) {
        this.bookingcontact = bookingcontact;
    }

    public String getBookingservice() {
        return bookingservice;
    }

    public void setBookingservice(String bookingservice) {
        this.bookingservice = bookingservice;
    }

    public String getBookingstatus() {
        return bookingstatus;
    }

    public void setBookingstatus(String bookingstatus) {
        this.bookingstatus = bookingstatus;
    }

    public String getRidername() {
        return ridername;
    }

    public void setRidername(String ridername) {
        this.ridername = ridername;
    }

    public String getRiderlat() {
        return riderlat;
    }

    public void setRiderlat(String riderlat) {
        this.riderlat = riderlat;
    }

    public String getRiderlong() {
        return riderlong;
    }

    public void setRiderlong(String riderlong) {
        this.riderlong = riderlong;
    }

    public String getBookingtimestamp() {
        return bookingtimestamp;
    }

    public void setBookingtimestamp(String bookingtimestamp) {
        this.bookingtimestamp = bookingtimestamp;
    }

    public String getBookinglat() {
        return bookinglat;
    }

    public void setBookinglat(String bookinglat) {
        this.bookinglat = bookinglat;
    }

    public String getBookinglong() {
        return bookinglong;
    }

    public void setBookinglong(String bookinglong) {
        this.bookinglong = bookinglong;
    }

    public String getBookingtotal() {
        return bookingtotal;
    }

    public void setBookingtotal(String bookingtotal) {
        this.bookingtotal = bookingtotal;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRiderid() {
        return riderid;
    }

    public void setRiderid(String riderid) {
        this.riderid = riderid;
    }

    public String getRidercontact() {
        return ridercontact;
    }

    public void setRidercontact(String ridercontact) {
        this.ridercontact = ridercontact;
    }
}
