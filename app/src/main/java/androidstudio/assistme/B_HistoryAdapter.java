package androidstudio.assistme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/27/2017.
 */

public class B_HistoryAdapter extends BaseAdapter {
    ArrayList<B_BookingClass> items;
    Context c;

    public B_HistoryAdapter(Context c, ArrayList<B_BookingClass> items) {
        this.c = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.orderhistoryrow, viewGroup, false);
        }

        final B_BookingClass o = (B_BookingClass) this.getItem(i);
        TextView tvid = (TextView) view.findViewById(R.id.orderid);
        TextView tvdate = (TextView) view.findViewById(R.id.orderdate);
        TextView tvtotal = (TextView) view.findViewById(R.id.ordertotal);

        tvid.setText("Booking ID: " +o.getBookingid());
        tvdate.setText("Booking Timestamp: " + o.getBookingtimestamp());
        tvtotal.setText("Booking Fee: " + o.getBookingtotal());
        return view;
    }
}