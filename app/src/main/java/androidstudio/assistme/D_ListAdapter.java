package androidstudio.assistme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/25/2017.
 */

public class D_ListAdapter extends BaseAdapter {
    ArrayList<D_MenuClass> items;
    ArrayList<D_MenuClass> itemshldrdel;
    Context c;
    int pos;


    public D_ListAdapter(Context c, ArrayList<D_MenuClass> items){
        this.c = c;
        this.items = items;
    }

    public void remove(int position, ArrayList<D_MenuClass> items){
        this.pos = position;
        this.itemshldrdel = items;
        itemshldrdel.remove(itemshldrdel.get(position));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.row, viewGroup, false);
        }

        final D_MenuClass m = (D_MenuClass) this.getItem(i);
        TextView tvname = (TextView) view.findViewById(R.id.itemname);
        TextView tvprice = (TextView) view.findViewById(R.id.itemprice);
        TextView tvquantity = (TextView) view.findViewById(R.id.itemquantity);

        tvname.setText("Item Name: " + m.getItemname());
        tvprice.setText("Item Price: " + m.getItemprice());
        tvquantity.setText("Item Quantity: "+ m.getItemquan());
        return view;
    }
}
