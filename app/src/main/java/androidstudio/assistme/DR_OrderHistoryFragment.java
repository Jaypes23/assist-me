package androidstudio.assistme;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 12/3/2017.
 */

public class DR_OrderHistoryFragment extends android.support.v4.app.Fragment {
    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    String USER_TYPE;
    D_OrderClass lm;
    ArrayList<D_OrderClass> listmenu;
    ArrayList<String> orderdetails;
    ArrayList<ArrayList<String>> itemsfromhistoryarray;
    ArrayList<String> itemhldr;
    public String isEmpty = "true";
    public String uid,orderid,orderdate,address,contact,total,changefor,latitude,longitude,nongeoloc,status;
    public String changeorderid, changeorderdate, changeorderadd, changeordercontact, changeordertotal, changeorderchange,
                  changeorderlat, changeorderlong, changerodernongeo, changeorderstatus;
    public String driverlat, driverlong;
    public ArrayList<String> items,changeitems;
    public String prefuid, prefemail, prefname, prefcontact, prefaddress, prefordertrans;
    LocationManager mLocationManager;
    Location myLocation;

    //UI
    @BindView(R.id.clienthistorylist)
    ListView orderlist;

    D_HistoryAdapter itemAdapter;

    MaterialDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pastorders, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        dialog = new MaterialDialog.Builder(getActivity())
                .title("Loading Orders")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        lm = new D_OrderClass();
        listmenu = new ArrayList<>();
        orderdetails = new ArrayList<>();
        itemsfromhistoryarray = new ArrayList<>();
        itemhldr = new ArrayList<>();
        changeitems = new ArrayList<>();
        items = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        itemAdapter = new D_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);
        RealtimeOrderHistory();

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_orderinfo, null);
                dialogBuilder.setView(dialogView);
                String valhldr = orderdetails.get(position);
                itemhldr = itemsfromhistoryarray.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[/]+";
                String[] tokens = parser.split(delims);
                String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordchangefor = String.valueOf(tokens[5]);

                final ListView listView = (ListView) dialogView.findViewById(R.id.deliveryitemlist);
                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);

                tvordernum.setText("Order ID: "+ordid);
                tvordtotal.setText("Order Total: "+ordtotal);
                tvordstatus.setText("Delivered");
                tvordaddress.setText("Your Delivery Address: "+ordadd);
                tvordcontact.setText("Your Contact Number: "+ordcon);
                tvordchange.setText("Requested Change For: "+ordchangefor);
                tvordtimestamp.setText("Delivery Timestamp: "+orddate);

                listView.setAdapter(new D_ListAdapter(getActivity(), ViewCartList()));
                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                // Cancel Button
                Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancellist);
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                    }
                });

                alertDialog.show();
            }});
        dialog.dismiss();
    }

    public void RealtimeOrderHistory(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final D_OrderClass order = dataSnapshot.getValue(D_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        status = order.getOrderstatus();
                        latitude = order.getLatitude();
                        longitude = order.getLongitude();
                        nongeoloc = order.getRealaddress();
                        items = order.getItems();
                        Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
                        if (status.equals("delivered")) {
                            lm = new D_OrderClass();
                            lm.setOrdernum(orderid);
                            lm.setTimestamp(orderdate);
                            lm.setAddress(address);
                            lm.setContactnum(contact);
                            lm.setCarttotal(total);
                            lm.setChangefor(changefor);
                            lm.setItems(items);
                            lm.setLatitude(latitude);
                            lm.setLongitude(longitude);
                            lm.setRealaddress(nongeoloc);
                            listmenu.add(lm);
                            orderdetails.add(orderid + "/" + orderdate + "/" + address + "/" + contact + "/" + total + "/" + changefor + "/" + latitude + "/" + longitude + "/" + nongeoloc);
                            itemsfromhistoryarray.add(items);
                            itemAdapter.notifyDataSetChanged();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final D_OrderClass order = dataSnapshot.getValue(D_OrderClass.class);
                        changeorderid = order.getOrdernum();
                        changeorderdate = order.getTimestamp();
                        changeorderadd = order.getAddress();
                        changeordercontact = order.getContactnum();
                        changeordertotal = order.getCarttotal();
                        changeorderchange = order.getChangefor();
                        changeorderstatus = order.getOrderstatus();
                        changeitems = order.getItems();
                        changeorderlat = order.getLatitude();
                        changeorderlong = order.getLongitude();
                        changerodernongeo = order.getRealaddress();
                        if (changeorderstatus.equals("delivered")) {
                            lm = new D_OrderClass();
                            lm.setOrdernum(changeorderid);
                            lm.setTimestamp(changeorderdate);
                            lm.setAddress(changeorderadd);
                            lm.setContactnum(changeordercontact);
                            lm.setCarttotal(changeordertotal);
                            lm.setChangefor(changeorderchange);
                            lm.setItems(changeitems);
                            lm.setLatitude(changeorderlat);
                            lm.setLongitude(changeorderlong);
                            lm.setRealaddress(changerodernongeo);
                            listmenu.add(lm);
                            orderdetails.add(changeorderid + "/" + changeorderdate + "/" + changeorderadd + "/" + changeordercontact + "/" + changeordertotal + "/" + changeorderchange + "/" + changeorderlat + "/" + changeorderlong + "/" + changerodernongeo);
                            itemsfromhistoryarray.add(changeitems);
                            itemAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private ArrayList ViewCartList(){
        ArrayList<D_MenuClass> listmenu = new ArrayList<>();
        D_MenuClass lm = new D_MenuClass();
        for(int i = 0; i<itemhldr.size();i++){
            lm = new D_MenuClass();
            String arrayvalhldr = itemhldr.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }
}
