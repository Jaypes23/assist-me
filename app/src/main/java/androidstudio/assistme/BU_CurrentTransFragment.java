package androidstudio.assistme;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/4/2017.
 */

public class BU_CurrentTransFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, ordertotal, orderlat, orderlong, orderstat, orderservice;
    String firebaseorderstatus;
    String ridername,ridercontact;
    Activity activity;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delservice)
    TextView tvorderservice;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.buttoncancel)
    Button btnCancel;
    @BindView(R.id.buttontrackorder)
    Button btnTrack;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookservicefound, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
        btnTrack.setEnabled(true);

        RetrieveBookingData();
        new RetrieveBookingStatus().execute();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ridercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ridercontact, null)));
            }
        });

        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FA_TrackRiderFragment.class);
                startActivity(intent);
            }
        });
    }

    public void RetrieveBookingData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("bookid", "ID");
        orderaddress = prefs.getString("bookaddress", "Address");
        ordercontact = prefs.getString("bookcontact", "Contact");
        ordertimestamp = prefs.getString("booktimestamp", "Timestamp");
        orderservice = prefs.getString("bookservice", "Service");
        ordertotal = prefs.getString("booktotal", "Total");
        orderlat = prefs.getString("booklatitude", "Latitude");
        orderlong = prefs.getString("booklongitude", "Longitude");
        orderstat = prefs.getString("bookstatus", "Status");

        tvorderid.setText("Booking ID: "+orderid);
        tvorderaddress.setText("Your Address: "+orderaddress);
        tvordertimestamp.setText("Booking Timestamp: "+ordertimestamp);
        tvordertotal.setText("Service Fee: "+ordertotal);
        tvorderstatus.setText("Service would be provided shortly");
        tvorderservice.setText("The Service you requested: "+orderservice);
    }

    class RetrieveBookingStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
            activity = getActivity();
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .child(orderid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final B_BookingClass book = dataSnapshot.getValue(B_BookingClass.class);
                            firebaseorderstatus = book.getBookingstatus();
                            ridername = book.getRidername();
                            ridercontact = book.getRidercontact();

                            if(firebaseorderstatus.equals("delivered")){
                                dialog = new MaterialDialog.Builder(activity)
                                        .title("Service Successful!")
                                        .content("Thank you for using Assist ME!")
                                        .positiveText("Close")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                editor.putString("booktransaction", "false");
                                                editor.putString("bookid", "none");
                                                editor.putString("bookaddress", "none");
                                                editor.putString("bookcontact", "none");
                                                editor.putString("booktimestamp", "none");
                                                editor.putString("booktotal", "none");
                                                editor.putString("booklatitude", "none");
                                                editor.putString("booklongitude", "none");
                                                editor.putString("bookservice", "none");
                                                editor.apply();

                                                dialog.dismiss();

                                                Fragment fragment = new BU_MainFragment();
                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.remove(new BU_CurrentTransFragment());
                                                fragmentTransaction.replace(R.id.frame_container, fragment);
                                                fragmentTransaction.commit();
                                            }
                                        })
                                        .show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }





}
