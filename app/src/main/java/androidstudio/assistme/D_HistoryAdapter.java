package androidstudio.assistme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/27/2017.
 */

public class D_HistoryAdapter extends BaseAdapter {
    ArrayList<D_OrderClass> items;
    Context c;

    public D_HistoryAdapter(Context c, ArrayList<D_OrderClass> items) {
        this.c = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.orderhistoryrow, viewGroup, false);
        }

        final D_OrderClass o = (D_OrderClass) this.getItem(i);
        TextView tvid = (TextView) view.findViewById(R.id.orderid);
        TextView tvdate = (TextView) view.findViewById(R.id.orderdate);
        TextView tvtotal = (TextView) view.findViewById(R.id.ordertotal);

        tvid.setText("Order ID: " +o.getOrdernum());
        tvdate.setText("Order Timestamp: " + o.getTimestamp());
        tvtotal.setText("Order Total: " + o.getCarttotal());
        return view;
    }
}