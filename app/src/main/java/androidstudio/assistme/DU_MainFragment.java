package androidstudio.assistme;

/**
 * Created by JP on 11/23/2017.
 */


import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 11/20/2017.
 */

public class DU_MainFragment extends android.support.v4.app.Fragment implements D_CartListener {

    //GridView
    @BindView(R.id.gvitems)
    GridView gv;

    //Preference
    private String USER_TYPE;
    String ordertransaction, uid, name, email, address, contact;

    //Buttons
    @BindView(R.id.btnCart)
    FancyButton btnShowCart;
    @BindView(R.id.btnCheckout)
    FancyButton btnCheckout;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    D_CardAdapter adapter;
    D_ListAdapter lvcartadapter;
    ArrayList<String> itemsarray;
    ArrayList<String> totalarray;
    Double carttotalval = 0.00;
    String arrayvalhldr;
    public String timestamp;
    public SimpleDateFormat currentTimeStamp;

    //Location Variables
    String faddress;
    double flatitude, flongitude;
    Address geolocaddfromlatlong;
    LocationManager mLocationManager;
    Location myLocation;


    Geocoder geocoder;
    List<Address> addresses;
    String latitude, longitude;

    public DU_MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize() {
        RetrieveUserData();
        if (ordertransaction.equals("true")) {
            Fragment fragment = new DU_CurrentTransFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.remove(new DU_MainFragment());
            fragmentTransaction.commit();
        } else {
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            itemsarray = new ArrayList<>();
            totalarray = new ArrayList<>();
            adapter = new D_CardAdapter(getActivity(), getData());
            adapter.setListener(this);
            gv.setAdapter(adapter);

            btnShowCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemsarray.size() != 0 && totalarray.size() != 0) {
                        carttotalval = 0.0;
                        GetCartTotal();
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_cart, null);
                        dialogBuilder.setView(dialogView);

                        final ListView listView = (ListView) dialogView.findViewById(R.id.cartlist);
                        final TextView textView = (TextView) dialogView.findViewById(R.id.carttotal);

                        textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));

                        lvcartadapter = new D_ListAdapter(getActivity(), ViewCartList());
                        listView.setAdapter(lvcartadapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                                adapter.removeItem(position);
//                                lvcartadapter.remove(position,ViewCartList());
//                                lvcartadapter.notifyDataSetChanged();
                            }
                        });


                        final AlertDialog alertDialog = dialogBuilder.create();
                        Window window = alertDialog.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        window.setGravity(Gravity.CENTER);

                        // Cancel Button
                        Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancellist);
                        cancel_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        alertDialog.show();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Cart is empty!")
                                .content("Select Items first!")
                                .positiveText("Ok")
                                .show();
                    }
                }
            });

            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemsarray.size() != 0 && totalarray.size() != 0) {
                        carttotalval = 0.0;
                        GetCartTotal();
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_checkout, null);
                        dialogBuilder.setView(dialogView);

                        final ListView listView = (ListView) dialogView.findViewById(R.id.checkoutcartlist);
                        final TextView textView = (TextView) dialogView.findViewById(R.id.checkoutcarttotal);
                        final EditText etaddress = (EditText) dialogView.findViewById(R.id.checkout_address);
                        final EditText etcontact = (EditText) dialogView.findViewById(R.id.checkout_contact);
                        final EditText etchange = (EditText) dialogView.findViewById(R.id.checkout_changefor);
                        final CheckedTextView ctvifuseradd = (CheckedTextView) dialogView.findViewById(R.id.checkifuseraddress);

                        etaddress.setText(address);
                        etcontact.setText(contact);

                        listView.setAdapter(new D_ListAdapter(getActivity(), ViewCartList()));
                        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int itemPosition = i;
                                String itemValue = (String) listView.getItemAtPosition(i);
                                Toast.makeText(getActivity(),
                                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                                        .show();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));

                        final AlertDialog alertDialog = dialogBuilder.create();
                        Window window = alertDialog.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        window.setGravity(Gravity.CENTER);

                        Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancel);
                        Button order_btn = (Button) dialogView.findViewById(R.id.buttonorder);
                        cancel_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });
                        order_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (TextUtils.isEmpty(etaddress.getText().toString())) {
                                    etaddress.setError("Required");
                                    return;
                                }
                                if (TextUtils.isEmpty(etcontact.getText().toString())) {
                                    etcontact.setError("Required");
                                    return;
                                }
                                if (TextUtils.isEmpty(etchange.getText().toString())) {
                                    etchange.setError("Required");
                                    return;
                                } else {
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                    LayoutInflater inflater = getActivity().getLayoutInflater();
                                    View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                    dialogBuilder.setView(dialogView);

                                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                                    Window window = OptionalertDialog.getWindow();
                                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    window.setGravity(Gravity.CENTER);

                                    final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                    final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                    final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                    optionheader.setText("Proceed now?");

                                    negative.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            OptionalertDialog.dismiss();
                                        }
                                    });

                                    positive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Date currentTime = Calendar.getInstance().getTime();
                                            DateFormat df = new SimpleDateFormat("MM-dd-yyyy-HH:mm:ss");
                                            String reportDate = df.format(currentTime);
                                            final int random = new Random().nextInt(991) + 10;
                                            String ordernum = "ORD-" + uid.substring(20) + random;
                                            D_OrderClass order;
                                            if (ctvifuseradd.isChecked()) {
                                                getLastKnownLocation();
                                                order = new D_OrderClass(ordernum, uid, etaddress.getText().toString(), etcontact.getText().toString(),
                                                        reportDate, etchange.getText().toString(), String.valueOf(carttotalval),
                                                        itemsarray, latitude, longitude, etaddress.getText().toString(), "pending");
                                                mDatabase.child("Orders").child(ordernum).setValue(order);
                                            } else {
                                                getLocationFromAddress(getActivity(), etaddress.getText().toString());
                                                order = new D_OrderClass(ordernum, uid, String.valueOf(geolocaddfromlatlong), etcontact.getText().toString(),
                                                        reportDate, etchange.getText().toString(), String.valueOf(carttotalval),
                                                        itemsarray, latitude, longitude, etaddress.getText().toString(), "pending");
                                                mDatabase.child("Orders").child(ordernum).setValue(order);
                                            }

                                            Toast.makeText(getActivity(), "Order sent!!", Toast.LENGTH_SHORT).show();

                                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            if (ctvifuseradd.isChecked()) {
                                                editor.putString("orderaddress", etaddress.getText().toString());
                                            } else {
                                                editor.putString("orderaddress", String.valueOf(geolocaddfromlatlong));
                                            }
                                            editor.putString("ordertransaction", "true");
                                            editor.putString("orderid", ordernum);
                                            editor.putString("ordercontact", etcontact.getText().toString());
                                            editor.putString("ordertimestamp", reportDate);
                                            editor.putString("orderchangefor", etchange.getText().toString());
                                            editor.putString("nongeolocadd", etaddress.getText().toString());
                                            editor.putString("orderlatitude", latitude);
                                            editor.putString("orderlongitude", longitude);
                                            editor.putString("orderstatus", "pending");
                                            editor.putString("ordertotal", String.valueOf(carttotalval));
                                            Set<String> set = new HashSet<String>();
                                            set.addAll(itemsarray);
                                            editor.putStringSet("orderitemarray", set);
                                            editor.apply();

                                            alertDialog.dismiss();
                                            OptionalertDialog.dismiss();

                                            Fragment fragment = new DU_CurrentTransFragment();
                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                            fragmentTransaction.replace(R.id.frame_container, fragment);
                                            fragmentTransaction.remove(new DU_MainFragment());
                                            fragmentTransaction.commit();
                                        }
                                    });
                                    OptionalertDialog.show();
                                }
                            }
                        });
                        alertDialog.show();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Cart is empty!")
                                .content("Select Items first!")
                                .positiveText("Ok")
                                .show();
                    }
                }
            });
        }

    }

    private ArrayList getData() {
        ArrayList<D_MenuClass> menu = new ArrayList<>();
        D_MenuClass m = new D_MenuClass();
        m.setItemname("Red Horse 500ml");
        m.setItemprice("₱44.00");
        m.setItemimage(R.drawable.red_horse500);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Red Horse Mucho");
        m.setItemprice("₱84.00");
        m.setItemimage(R.drawable.red_horsemucho);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Pale Pilsen");
        m.setItemprice("₱34.00");
        m.setItemimage(R.drawable.pilsen);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Pale Grande");
        m.setItemprice("₱84.00");
        m.setItemimage(R.drawable.grande);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("San Mig Light");
        m.setItemprice("₱34.00");
        m.setItemimage(R.drawable.sanmiglight);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Tanduay Ice");
        m.setItemprice("₱34.00");
        m.setItemimage(R.drawable.tanduayice);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Gin Bilog");
        m.setItemprice("₱49.00");
        m.setItemimage(R.drawable.ginbilog);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Quatro Kintos");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.ginquatro);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Tanduay Jr");
        m.setItemprice("₱44.00");
        m.setItemimage(R.drawable.tanduayjr);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Tanduay Sr");
        m.setItemprice("₱54.00");
        m.setItemimage(R.drawable.tanduaysr);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Emperador 350ml");
        m.setItemprice("₱59.00");
        m.setItemimage(R.drawable.emperador350ml);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Emperador 500ml");
        m.setItemprice("₱79.00");
        m.setItemimage(R.drawable.emperador500ml);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Emperador 750ml");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.emperador750ml);
        menu.add(m);

        m = new D_MenuClass();
        m.setItemname("Emperador 1 Liter");
        m.setItemprice("₱129.00");
        m.setItemimage(R.drawable.emperador1lt);
        menu.add(m);

        return menu;
    }

    private ArrayList ViewCartList() {
        ArrayList<D_MenuClass> listmenu = new ArrayList<>();
        D_MenuClass lm = new D_MenuClass();
        for (int i = 0; i < itemsarray.size(); i++) {
            lm = new D_MenuClass();
            arrayvalhldr = itemsarray.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return listmenu;
    }

    public void GetCartTotal() {
        for (int c = 0; c < totalarray.size(); c++) {
            carttotalval = Double.parseDouble(totalarray.get(c)) + carttotalval;
        }
    }

    @Override
    public void AddtoCart(String itemcart, String total) {
        itemsarray.add(itemcart);
        totalarray.add(total);
    }

    @Override
    public void UpdateCart(int index, String itemcart, String total) {
        itemsarray.set(index, itemcart);
        totalarray.set(index, total);
    }

    @Override
    public void RemovefromCart(int index) {
        itemsarray.remove(index);
        totalarray.remove(index);
    }

    public void RetrieveUserData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("ordertransaction", "order");
        uid = prefs.getString("userid", "UID");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            } else {
                Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocationGPS != null) {
                    return lastKnownLocationGPS;
                } else {
                    Location loc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    System.out.println("1::" + loc);
                    System.out.println("2::" + loc.getLatitude());
                    System.out.println("3::" + loc.getLongitude());

                    latitude = String.valueOf(loc.getLatitude());
                    longitude = String.valueOf(loc.getLongitude());

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                        Toast.makeText(getActivity(), address + city + state + country + postalCode + knownName, Toast.LENGTH_SHORT).show();
                        faddress = address;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return loc;
                }
            }
        } else {
            return null;
        }
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    latitude = String.valueOf(bestLocation.getLatitude());
                    longitude = String.valueOf(bestLocation.getLongitude());

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                        Toast.makeText(getActivity(), address + city + state + country + postalCode + knownName, Toast.LENGTH_SHORT).show();
                        faddress = address;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return bestLocation;
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            } else {
                try {
                    address = coder.getFromLocationName(strAddress, 5);
                    if (address == null) {
                        Toast.makeText(context, "Address is Invalid Try again!", Toast.LENGTH_SHORT).show();
                        return null;
                    }
                    geolocaddfromlatlong = address.get(0);
                    latitude = String.valueOf(geolocaddfromlatlong.getLatitude());
                    longitude = String.valueOf(geolocaddfromlatlong.getLongitude());

                    p1 = new LatLng(geolocaddfromlatlong.getLatitude(), geolocaddfromlatlong.getLongitude());
                    Toast.makeText(context, String.valueOf(p1), Toast.LENGTH_SHORT).show();

                } catch (IOException ex) {

                    ex.printStackTrace();
                }
                return p1;
            }
        }else{
            return null;
        }
    }
}

