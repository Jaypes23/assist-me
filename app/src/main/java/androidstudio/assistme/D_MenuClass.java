package androidstudio.assistme;

/**
 * Created by JP on 11/23/2017.
 */

public class D_MenuClass {
    int itemimage;
    String itemname;
    String itemprice;
    int itemquan;

    public D_MenuClass(){

    }

    public D_MenuClass(int itemimage, String itemname, String itemprice, int itemquan) {
        this.itemimage = itemimage;
        this.itemname = itemname;
        this.itemprice = itemprice;
        this.itemquan = itemquan;
    }

    public int getItemimage() {
        return itemimage;
    }

    public void setItemimage(int itemimage) {
        this.itemimage = itemimage;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemprice() {
        return itemprice;
    }

    public void setItemprice(String itemprice) {
        this.itemprice = itemprice;
    }

    public int getItemquan() {
        return itemquan;
    }

    public void setItemquan(int itemquan) {
        this.itemquan = itemquan;
    }
}
