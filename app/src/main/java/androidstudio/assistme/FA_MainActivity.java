package androidstudio.assistme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FA_MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //Variables
    String ordertransaction,name,email,address,contact;

    //Preference
    private String USER_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(FA_MainActivity.this);
        setSupportActionBar(toolbar);

        RetrieveUserData();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final FA_FirebaseClass FAFirebaseClass = new FA_FirebaseClass(FA_MainActivity.this);
            FAFirebaseClass.getUserInstance().signOut();
            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
            editor.remove("status");
            editor.remove("timehldr");
            editor.remove("username");
            editor.remove("useremail");
            editor.remove("useraddress");
            editor.remove("usercontact");
            editor.commit();
            Intent intent = new Intent(FA_MainActivity.this, FA_LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_beer) {
            if(ordertransaction.equals("false")){
                fragment = new BU_MainFragment();
                toolbar.setTitle("Services");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
            else if(ordertransaction.equals("true")){
                fragment = new BU_CurrentTransFragment();
                toolbar.setTitle("Service in Progress");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
        } else if (id == R.id.nav_history) {
            fragment = new BU_BookHistoryFragment();
            toolbar.setTitle("History");
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        } else if (id == R.id.nav_profile) {
            fragment = new FA_ProfileFragment();
            toolbar.setTitle("Profile");
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void RetrieveUserData(){
        SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

}



