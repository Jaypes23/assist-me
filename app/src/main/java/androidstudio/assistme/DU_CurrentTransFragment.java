package androidstudio.assistme;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 11/27/2017.
 */

public class DU_CurrentTransFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, orderchangefor, ordertotal, orderlat, orderlong, ordernongeoadd, orderstat;
    String firebaseorderstatus;
    ArrayList<String> orderitemsarray;
    String arrayitemshldr;
    Activity activity;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delchangefor)
    TextView tvorderchangefor;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.deliveryitemlist)
    ListView lvitemlist;
    @BindView(R.id.buttoncancel)
    Button btnCancel;
    @BindView(R.id.buttontrackorder)
    Button btnTrack;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;

    public DU_CurrentTransFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
        new RetrieveOrderStatus().execute();
        RetrieveOrderData();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09279318248", null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", "09279318248", null)));
            }
        });

        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FA_TrackRiderFragment.class);
                startActivity(intent);
            }
        });
    }

    public void RetrieveOrderData(){
        orderitemsarray = new ArrayList<>();
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("orderid", "ID");
        orderaddress = prefs.getString("orderaddress", "Address");
        ordercontact = prefs.getString("ordercontact", "Contact");
        ordertimestamp = prefs.getString("ordertimestamp", "Timestamp");
        orderchangefor = prefs.getString("orderchangefor", "ChangeFor");
        ordertotal = prefs.getString("ordertotal", "Total");
        orderlat = prefs.getString("orderlatitude", "Latitude");
        orderlong = prefs.getString("orderlongitude", "Longitude");
        orderstat = prefs.getString("orderstatus", "Status");
        ordernongeoadd = prefs.getString("nongeolocadd", "NonGeoAdd");
        Set<String> set = prefs.getStringSet("orderitemarray", null);
        orderitemsarray = new ArrayList<String>(set);

        tvorderid.setText("Order ID: "+orderid);
        tvorderaddress.setText("Your Delivery Address: "+orderaddress);
        tvordercontact.setText("Your Contact Number: "+ordercontact);
        tvordertimestamp.setText("Delivery Timestamp: "+ordertimestamp);
        tvordertotal.setText("Order Total: "+ordertotal);
        tvorderchangefor.setText("Requested Change For: "+orderchangefor);
        tvorderstatus.setText("Items would be delivered shortly");

        lvitemlist.setAdapter(new D_ListAdapter(getActivity(), ViewCartList()));
        lvitemlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int itemPosition = i;
                String  itemValue = (String) lvitemlist.getItemAtPosition(i);
                Toast.makeText(getActivity(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private ArrayList ViewCartList(){
        ArrayList<D_MenuClass> listmenu = new ArrayList<>();
        D_MenuClass lm = new D_MenuClass();
        for(int i = 0; i<orderitemsarray.size();i++){
            lm = new D_MenuClass();
            arrayitemshldr = orderitemsarray.get(i);
            String parser = arrayitemshldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    class RetrieveOrderStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
            activity = getActivity();
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Orders")
                    .child(orderid)
                    .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final D_OrderClass order = dataSnapshot.getValue(D_OrderClass.class);
                    firebaseorderstatus = order.getOrderstatus();
                    if(firebaseorderstatus.equals("delivered")){
                        dialog = new MaterialDialog.Builder(activity)
                                .title("Delivery Successful!")
                                .content("Thank you for using Beer 2 Go!")
                                .positiveText("Close")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        editor.putString("ordertransaction", "false");
                                        editor.putString("orderid", "none");
                                        editor.putString("orderaddress", "none");
                                        editor.putString("ordercontact", "none");
                                        editor.putString("ordertimestamp", "none");
                                        editor.putString("orderchangefor", "none");
                                        editor.putString("ordertotal", "none");
                                        editor.putString("orderlatitude", "none");
                                        editor.putString("orderlongitude", "none");
                                        editor.putString("nongeolocadd", "none");

                                        orderitemsarray = new ArrayList<>();
                                        Set<String> set = new HashSet<String>();
                                        set.addAll(orderitemsarray);
                                        editor.putStringSet("orderitemarray", set);
                                        editor.apply();

                                        dialog.dismiss();

                                        Fragment fragment = new DU_MainFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.remove(new DU_CurrentTransFragment());
                                        fragmentTransaction.replace(R.id.frame_container, fragment);
                                        fragmentTransaction.commit();
                                    }
                                })
                                .show();
                    }
                    if(firebaseorderstatus.equals("otw")){
                        if (dialog == null) {
                            dialog = new MaterialDialog.Builder(activity)
                                    .title("Delivery Update!")
                                    .content("Rider is on the way!")
                                    .positiveText("Close")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            tvorderstatus.setText("Items would be delivered shortly. Rider is on the way");
                                            btnTrack.setEnabled(true);
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }
}
