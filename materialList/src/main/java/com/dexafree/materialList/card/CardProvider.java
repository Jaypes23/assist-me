package com.dexafree.materialList.card;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dexafree.materialList.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * The basic CardProvider.
 */
public class CardProvider<T extends CardProvider> extends Observable {
    private final static int DIVIDER_MARGIN_DP = 16;

    private Context mContext;
    private Card.Builder mBuilder;

    private String mId;
    private String mImageUri;
    private String mTitle;
    private String mSubtitle;
    private String mDescription;
    private String mRatingScore;
    private String mPublished;
    private String mEditions;
    private String mRatings;
    private String mStatus;
    private String mName;
    private String mContent;
    private String mProfilePic;
    private String mTimestamp;
    private String mBookName;
    private boolean mDividerVisible;
    private boolean mFullWidthDivider;
    private int mTitleGravity;
    private int mSubtitleGravity;
    private int mDescriptionGravity;
    private float FONT_SIZE = 13;
    byte[] decodeImageProfileBytes, decodeImagePostBytes;
    Bitmap bitmapProfile, bitmapPost;


    private String mleaveappdate;
    private String mleavestart;
    private String mleaveend;
    private String mleavereason;
    private String mleavetotaldays;
    private String mleavetype;
    private String mleaveid;
    private String mleavestatus;

    private String mleaveappdatehldr;
    private String mleavestarthldr;
    private String mleaveendhldr;
    private String mleavereasonhldr;
    private String mleavetotaldayshldr;
    private String mleavetypehldr;
    private String mleaveidhldr;
    private String mleavestatushldr;

    private String chatname;
    private String chatnamehldr;
    private String chatid;
    private String chatidhldr;

    private String compname;
    private String compnamehldr;
    private String compdesc;
    private String compdeschldr;
    private String compkey;
    private String compkeyhldr;



    @ColorInt
    private int mBackgroundColor = Color.WHITE;
    @ColorInt
    private int mTitleColor;
    @ColorInt
    private int mSubtitleColor;
    @ColorInt
    private int mDescriptionColor;
    @ColorInt
    private int mDividerColor;
    @Nullable
    private Drawable mDrawable;
    @Nullable
    private String mUrlImage;
    @Nullable
    private String mPostImage;
    private final Map<Integer, Action> mActionMapping = new HashMap<>();

    private OnImageConfigListener mOnImageConfigListenerListener;
    private int mLayoutId;


    /////////////////////////////////////////////////////////////////
    //
    //      Functions related to the builder pattern.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * Do NOT use this method! Only for the {@code Card.Builder}!
     *
     * @param context to access the resources.
     */
    void setContext(Context context) {
        mContext = context;
        onCreated();
    }

    /**
     * Do NOT use this method! Only for the {@code Card.Builder}!
     *
     * @param builder to return the {@code Card.Builder} by {@code endConfig}.
     */
    void setBuilder(Card.Builder builder) {
        mBuilder = builder;
    }

    protected void onCreated() {
        //setTitleResourceColor(R.color.grey_title);
        setDescriptionResourceColor(R.color.description_color);
    }

    /**
     * Get the context.
     *
     * @return the context.
     */
    protected Context getContext() {
        return mContext;
    }

    /**
     * End withProvider the configuration.
     *
     * @return the {@code Card.Builder}.
     */
    public Card.Builder endConfig() {
        return mBuilder;
    }

    /**
     * Notifies the Card that the content changed.
     */
    protected void notifyDataSetChanged() {
        notifyDataSetChanged(null);
    }

    /**
     * Notifies the Card that the content changed.
     */
    protected void notifyDataSetChanged(@Nullable final Object object) {
        setChanged();
        notifyObservers(object);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Functions related to setting and getting the properties.
    //
    /////////////////////////////////////////////////////////////////

    @NonNull
    @SuppressWarnings("unchecked")
    public T setLayout(@LayoutRes final int layoutId) {
        mLayoutId = layoutId;
        return (T) this;
    }

    /**
     * Get the card layout as resource.
     *
     * @return the card layout.
     */
    @LayoutRes
    public int getLayout() {
        return mLayoutId;
    }

    /**
     * Get the background color.
     *
     * @return the background color.
     */
    @ColorInt
    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    /**
     * Set the background color withProvider an real color (e.g. {@code Color.WHITE}).
     *
     * @param color as real.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setBackgroundColor(@ColorInt final int color) {
        mBackgroundColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public T setPostImage(@NonNull final String image) {
        mPostImage = image;
        notifyDataSetChanged();
        return (T) this;
    }
    /**
     * Set the background color withProvider an resource color (e.g. {@code
     * android.R.color.white}).
     *
     * @param color as resource.
     * @return the renderer.
     */
    @NonNull
    public T setBackgroundResourceColor(@ColorRes final int color) {
        return setBackgroundColor(getContext().getResources().getColor(color));
    }

    /**
     * Get the title.
     *
     * @return the title.
     */

    /**
     * Set the title withProvider a string resource.
     *
     * @param title to set.
     * @return the renderer.
     */
    @NonNull
    public T setTitle(@StringRes final int title) {
        return setTitle(getContext().getString(title));
    }
    /**
     * Set the title.
     *
     * @param title to set.
     * @return the renderer.
     */
    @SuppressWarnings("unchecked")
    public T setTitle(@NonNull final String title) {
        mTitle = title;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveId(@NonNull final String leaveId) {
        mleaveid = leaveId;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveIdhldr(@NonNull final String leaveIdhldr) {
        mleaveid = leaveIdhldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveAppDate(@NonNull final String leaveAppDate) {
        mleaveappdate = leaveAppDate;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveAppDatehldr(@NonNull final String leaveAppDatehldr) {
        mleaveappdatehldr = leaveAppDatehldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveReason(@NonNull final String reason) {
        mleavereason = reason;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveReasonhldr(@NonNull final String reasonhldr) {
        mleavereasonhldr = reasonhldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveStart(@NonNull final String leaveStart) {
        mleavestart = leaveStart;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveStarthldr(@NonNull final String leaveStarthldr) {
        mleavestarthldr = leaveStarthldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveEnd(@NonNull final String leaveEnd) {
        mleaveend = leaveEnd;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveEndhldr(@NonNull final String leaveEndhldr) {
        mleaveendhldr = leaveEndhldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveTotalDays(@NonNull final String leaveTotalDays) {
        mleavetotaldays = leaveTotalDays;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveTotalDayshldr(@NonNull final String leaveTotalDayshldr) {
        mleavetotaldayshldr = leaveTotalDayshldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveStatus(@NonNull final String leaveStatus) {
        mleavestatus = leaveStatus;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveStatushldr(@NonNull final String leaveStatushldr) {
        mleavestatushldr = leaveStatushldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveType(@NonNull final String leaveType) {
        mleavetype = leaveType;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setLeaveTypehldr(@NonNull final String leavetypehldr) {
        mleavetypehldr = leavetypehldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setChatName(@NonNull final String chatName) {
        chatname = chatName;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setChatNamehldr(@NonNull final String chatNamehldr) {
        chatnamehldr = chatNamehldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setChatId(@NonNull final String chatId) {
        chatid = chatId;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setChatIdhldr(@NonNull final String chatIdhldr) {
        chatidhldr = chatIdhldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompName(@NonNull final String compName) {
        compname = compName;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompNamehldr(@NonNull final String compNamehldr) {
        compnamehldr = compNamehldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompDesc(@NonNull final String compDesc) {
        compdesc = compDesc;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompDeschldr(@NonNull final String compDeschldr) {
        compdeschldr = compDeschldr;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompKey(@NonNull final String compKey) {
        compkey = compKey;
        notifyDataSetChanged();
        return (T) this;
    }

    public T setCompKeyhldr(@NonNull final String compKeyhldr) {
        compkeyhldr = compKeyhldr;
        notifyDataSetChanged();
        return (T) this;
    }



    @SuppressWarnings("unchecked")
    public T setStatus(@NonNull final String status) {
        mStatus = status;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set Gravity of title.
     *
     * @param titleGravity
     * @return the renderer.
     */

    @NonNull
    @SuppressWarnings("unchecked")
    public T setTitleGravity(final int titleGravity) {
        mTitleGravity = titleGravity;
        notifyDataSetChanged();
        return (T) this;
    }
    /**
     * Get the subtitle.
     *
     * @return the subtitle.
     */
    public String getSubtitle() {
        return mSubtitle;
    }

    /**
     * Set the subtitle as resource.
     *
     * @param subtitle to set.
     * @return the renderer.
     */
    @NonNull
    public T setSubtitle(@StringRes final int subtitle) {
        return setSubtitle(getContext().getString(subtitle));
    }

    /**
     * Set the subtitle.
     *
     * @param subtitle to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setSubtitle(final String subtitle) {
        mSubtitle = subtitle;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set Gravity of subtitle
     *
     * @param subtitleGravity
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setSubtitleGravity(final int subtitleGravity) {
        mSubtitleGravity = subtitleGravity;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the description.
     *
     * @return the description.
     */
    public String getDescription() {
        return mDescription;
    }
    public String getRatingScore() {
        return mRatingScore;
    }
    public String getPublished() {
        return mPublished;
    }
    public String getEditions() {
        return mEditions;
    }
    public String getRatings() {
        return mRatings;
    }
    public String getId() {
        return mId;
    }
    public String getImageUri() {
        return mImageUri;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getStatus(){
        return mStatus;
    }
    public String getName(){
        return mName;
    }
    public String getContent(){
        return mContent;
    }
    public String getProfilePic(){
        return mProfilePic;
    }
    public String getTimestamp(){
        return mTimestamp;
    }
    public String getPostImage(){
        return mPostImage;
    }
    public String getBookName(){
        return mBookName;
    }
    public String getMleaveappdate() {
        return mleaveappdate;
    }
    public String getMleavestart() {
        return mleavestart;
    }
    public String getMleaveend() {
        return mleaveend;
    }
    public String getMleavereason() {
        return mleavereason;
    }
    public String getMleavetotaldays() {
        return mleavetotaldays;
    }
    public String getMleavetype() {
        return mleavetype;
    }
    public String getMleaveid() {
        return mleaveid;
    }
    public String getMleavestatus() {
        return mleavestatus;
    }
    public String getMleaveappdatehldr() {
        return mleaveappdatehldr;
    }
    public String getMleavestarthldr() {
        return mleavestarthldr;
    }
    public String getMleaveendhldr() {
        return mleaveendhldr;
    }
    public String getMleavereasonhldr() {
        return mleavereasonhldr;
    }
    public String getMleavetotaldayshldr() {
        return mleavetotaldayshldr;
    }
    public String getMleavetypehldr() {
        return mleavetypehldr;
    }
    public String getMleaveidhldr() {
        return mleaveidhldr;
    }
    public String getMleavestatushldr() {
        return mleavestatushldr;
    }

    public String getChatname(){
        return chatname;
    }
    public String getChatnamehldr(){
        return chatnamehldr;
    }

    public String getChatid(){
        return chatid;
    }
    public String getChatidhldr(){
        return chatidhldr;
    }

    public String getCompkeyhldr() {return compkeyhldr;}
    public String getCompkey() {return compkey;}
    public String getCompdeschldr() {return compdeschldr;}
    public String getCompdesc() {return compdesc;}
    public String getCompnamehldr() {return compnamehldr;}
    public String getCompname() {return compname;}



    @NonNull
    @SuppressWarnings("unchecked")
    public T setId(@NonNull final String id) {
        mId = id;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setImageUri(@NonNull final String imageUri) {
        mImageUri = imageUri;
        notifyDataSetChanged();
        return (T) this;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public T setRatingScore(@NonNull final String ratingScore) {
        mRatingScore = ratingScore;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setPublished(@NonNull final String published) {
        mPublished = published;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setEditions(@NonNull final String editions) {
        mEditions = editions;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setRatings(@NonNull final String ratings) {
        mRatings = ratings;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setContent(@NonNull final String content) {
        mContent = content;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setName(@NonNull final String name) {
        mName = name;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setTimestamp(@NonNull final String timestamp) {
        mTimestamp = timestamp;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setProfilePic(@NonNull final String profilePic) {
        mProfilePic = profilePic;
        notifyDataSetChanged();
        return (T) this;
    }
    @NonNull
    @SuppressWarnings("unchecked")
    public T setBookName(@NonNull final String bookname) {
        mBookName = bookname;
        notifyDataSetChanged();
        return (T) this;
    }




    /**
     * Set the description withProvider a string resource.
     *
     * @param description to set.
     * @return the renderer.
     */
    @NonNull
    public T setDescription(@StringRes final int description) {
        return setDescription(getContext().getString(description));
    }

    /**
     * Set the description.
     *
     * @param description to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDescription(@NonNull final String description) {
        mDescription = description;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     *Set Gravity of description
     *
     * @param descriptionGravity to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDescriptionGravity(final int descriptionGravity) {
        mDescriptionGravity = descriptionGravity;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the divider color as int.
     *
     * @return the divider color.
     */
    @ColorInt
    public int getDividerColor() {
        return mDividerColor;
    }

    /**
     * Set the divider color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    public T setDividerResourceColor(@ColorRes final int color) {
        return setDividerColor(getContext().getResources().getColor(color));
    }

    /**
     * Set the divider color as int.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDividerColor(@ColorInt final int color) {
        mDividerColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the drawable.
     *
     * @return the drawable.
     */
    public Drawable getDrawable() {
        return mDrawable;
    }

    /**
     * Set the drawable withProvider a drawable resource.
     *
     * @param drawable to set.
     * @return the renderer.
     */
    @NonNull
    public T setDrawable(@DrawableRes final int drawable) {
        return setDrawable(Uri.parse("android.resource://" + getContext().getPackageName()
                + "/" + drawable).toString());
    }

    /**
     * Set the drawable. This drawable can not be configured inside of the ImageView. It will
     * directly be drawn. If the configuration of the image is necessary, use {@link
     * #setDrawable(int)} or {@link #setDrawable(String)} and {@link
     * #setDrawableConfiguration(OnImageConfigListener)} to configure the render process.
     *
     * @param drawable to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDrawable(@Nullable final Drawable drawable) {
        mDrawable = drawable;
        notifyDataSetChanged();
        return (T) this;
    }



    /**
     * Set the drawable withProvider a web url.
     *
     * @param urlImage to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDrawable(@Nullable final String urlImage) {
        mUrlImage = urlImage;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the web url.
     *
     * @return the url.
     */
    public String getImageUrl() {
        return mUrlImage;
    }

    /**
     * Get the title color as int.
     *
     * @return the color.
     */
    @ColorInt
    public int getTitleColor() {
        return mTitleColor;
    }

    /**
     * Get the title gravity as int.
     *
     * @return the gravity.
     */

    public int getTitleGravity(){
        return mTitleGravity;
    }

    /**
     * Set the title color as int.
     *
     * @param color to set as int.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setTitleColor(@ColorInt final int color) {
        mTitleColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the title color as resource.
     *
     * @param color to set as resource.
     * @return the renderer.
     */
//    @NonNull
//    public T setTitleResourceColor(@ColorRes final int color) {
//        return setTitleColor(getContext().getResources().getColor(color));
//    }

    /**
     * Get the subtitle color as int.
     *
     * @return the subtitle color.
     */
    @ColorInt
    public int getSubtitleColor() {
        return mSubtitleColor;
    }

    /**
     * Get the subtitle gravity as int.
     *
     * @return the subtitle gravity.
     */
    public int getSubtitleGravity(){
        return mSubtitleGravity;
    }

    /**
     * Set the subtitle color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    public T setSubtitleResourceColor(@ColorRes final int color) {
        return setSubtitleColor(getContext().getResources().getColor(color));
    }

    /**
     * Set the subtitle color as int.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setSubtitleColor(@ColorInt final int color) {
        mSubtitleColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the description color as int.
     *
     * @return the color.
     */
    @ColorInt
    public int getDescriptionColor() {
        return mDescriptionColor;
    }

    /**
     * Get the description gravity as int.
     *
     * @return the gravity.
     */
    public int getDescriptionGravity() {
        return mDescriptionGravity;
    }

    /**
     * Set the description color as int.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDescriptionColor(@ColorInt final int color) {
        mDescriptionColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the description color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */
    @NonNull
    public T setDescriptionResourceColor(@ColorRes final int color) {
        return setDescriptionColor(getContext().getResources().getColor(R.color.colorPrimaryDark));
    }

    /**
     * Set the listener for image customizations.
     *
     * @param listener to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDrawableConfiguration(@NonNull final OnImageConfigListener listener) {
        mOnImageConfigListenerListener = listener;
        return (T) this;
    }

    /**
     * Get the listener.
     *
     * @return the listener.
     */
    public OnImageConfigListener getOnImageConfigListenerListener() {
        return mOnImageConfigListenerListener;
    }

    /**
     * Get the visibility state of the divider.
     *
     * @return the visibility state of the divider.
     */
    public boolean isDividerVisible() {
        return mDividerVisible;
    }

    /**
     * Set the divider visible or invisible.
     *
     * @param visible to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setDividerVisible(final boolean visible) {
        mDividerVisible = visible;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the state of wideness of the divider.
     *
     * @return the wideness of the divider.
     */
    public boolean isFullWidthDivider() {
        return mFullWidthDivider;
    }

    /**
     * Set the wideness of the divider to full.
     *
     * @param fullWidthDivider to set.
     * @return the renderer.
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T setFullWidthDivider(final boolean fullWidthDivider) {
        mFullWidthDivider = fullWidthDivider;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     *
     * @param actionViewId
     * @param action
     * @return
     */
    @NonNull
    @SuppressWarnings("unchecked")
    public T addAction(@IdRes final int actionViewId, @NonNull final Action action) {
        mActionMapping.put(actionViewId, action);
        return (T) this;
    }

    /**
     *
     * @param actionViewId
     * @return
     */
    @Nullable
    public Action getAction(@IdRes final int actionViewId) {
        return mActionMapping.get(actionViewId);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Functions for rendering.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * Renders the content and style of the card to the view.
     *
     * @param view to display the content and style on.
     * @param card to render.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("unchecked")
    public void render(@NonNull final View view, @NonNull final Card card) {
        // The card background
        final CardView cardView = findViewById(view, R.id.cardView, CardView.class);
        if (cardView != null) {
            cardView.setCardBackgroundColor(getBackgroundColor());
        }

        // Title
        final TextView title = findViewById(view, R.id.email, TextView.class);
        if (title != null) {
            title.setText(getTitle());
            title.setTextColor(view.getResources().getColor(R.color.colorPrimaryDark));
            title.setGravity(getTitleGravity());
            Typeface custom_font = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/Raleway-Black.ttf");
            title.setTypeface(custom_font);
        }

        // Subtitle
        final TextView subtitle = findViewById(view, R.id.subtitle, TextView.class);
        if (subtitle != null) {
            subtitle.setText(getSubtitle());
            subtitle.setTextColor(getSubtitleColor());
            subtitle.setGravity(getSubtitleGravity());
            if (getSubtitle() == null || getSubtitle().isEmpty()) {
                subtitle.setVisibility(View.GONE);
            } else {
                subtitle.setVisibility(View.VISIBLE);
            }
        }

        // Description
        final TextView supportingText = findViewById(view, R.id.accountid, TextView.class);
        if (supportingText != null) {
            supportingText.setText(getDescription());
            supportingText.setTextColor(getDescriptionColor());
            supportingText.setGravity(getDescriptionGravity());
        }

        final TextView ratingScore = findViewById(view, R.id.rateScore, TextView.class);
        final TextView ratingText = findViewById(view, R.id.average, TextView.class);
        if (ratingScore != null) {
            ratingScore.setText(getRatingScore());
            ratingText.setText("average rating");
//            ratingScore.setTextColor(getDescriptionColor());
//            ratingScore.setGravity(getDescriptionGravity());
        }
        final TextView published = findViewById(view, R.id.textPublished, TextView.class);
        if (published != null) {
            published.setText(getPublished());
            published.setTextSize(FONT_SIZE);
//            ratingScore.setTextColor(getDescriptionColor());
//            ratingScore.setGravity(getDescriptionGravity());
        }
        final TextView editions = findViewById(view, R.id.textEditions, TextView.class);
        if (editions != null) {
            editions.setText(getEditions());
            editions.setTextSize(FONT_SIZE);
//            ratingScore.setTextColor(getDescriptionColor());
//            ratingScore.setGravity(getDescriptionGravity());
        }
        final TextView ratings = findViewById(view, R.id.textRatings, TextView.class);
        if (ratings != null) {
            ratings.setText(getRatings());
            ratings.setTextSize(FONT_SIZE);
//            ratingScore.setTextColor(getDescriptionColor());
//            ratingScore.setGravity(getDescriptionGravity());
        }
        final TextView timestamp = findViewById(view, R.id.timestamp, TextView.class);
        if (timestamp != null) {
            timestamp.setText(getTimestamp());
            timestamp.setTextSize(FONT_SIZE);
//            ratingScore.setTextColor(getDescriptionColor());
//            ratingScore.setGravity(getDescriptionGravity());
        }
        final TextView name = findViewById(view, R.id.statusname, TextView.class);
        if (name != null) {
            name.setText(getName());
        }
        final TextView content = findViewById(view, R.id.mainpost, TextView.class);
        if (content != null) {
            content.setText(getContent());

        }
        final TextView bookname = findViewById(view, R.id.bookname, TextView.class);
        if (bookname != null) {
            bookname.setText(getBookName());
            Typeface custom_font = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/Raleway-Black.ttf");
            bookname.setTypeface(custom_font);
            bookname.setTextSize(FONT_SIZE);
        }

        final ImageView imageView = findViewById(view, R.id.image, ImageView.class);
        if (imageView != null) {
            if (getDrawable() != null) {
                imageView.setImageDrawable(getDrawable());
            } else {
                final RequestCreator requestCreator = Picasso.with(getContext())
                        .load(getImageUrl());
                if (getOnImageConfigListenerListener() != null) {
                    getOnImageConfigListenerListener().onImageConfigure(requestCreator);
                }
                requestCreator.into(imageView);
            }
        }
        final CircleImageView profilepic = findViewById(view, R.id.profile_image, CircleImageView.class);
        if (profilepic != null) {
            if(getProfilePic() != null) {
                try{
                    Picasso.with(mContext).load(getProfilePic()).into(profilepic);
                }
                catch (Exception e){
                }
            }
        }
        final ImageView postImage = findViewById(view, R.id.postImage, ImageView.class);
        if(postImage != null){
            if(getPostImage() != null) {
                try {
                    Picasso.with(mContext).load(getPostImage()).into(postImage);
                }
                catch (Exception e)
                {}
            }
        }



        // Divider
        final View divider = findViewById(view, R.id.divider, View.class);
        if (divider != null) {
            divider.setVisibility(isDividerVisible() ? View.VISIBLE : View.INVISIBLE);

            // After setting the visibility, we prepare the divider params
            // according to the preferences
            if (isDividerVisible()) {
                // If the divider has to be from side to side, the margin will be 0
                final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)
                        divider.getLayoutParams();
                if (isFullWidthDivider()) {
                    params.setMargins(0, 0, 0, 0);
                } else {
                    int dividerMarginPx = dpToPx(DIVIDER_MARGIN_DP);
                    // Set the margin
                    params.setMargins(
                            dividerMarginPx,
                            0,
                            dividerMarginPx,
                            0
                    );
                }
            }
        }

        // Actions
        for (final Map.Entry<Integer, Action> entry : mActionMapping.entrySet()) {
            final View actionViewRaw = findViewById(view, entry.getKey(), View.class);
            if (actionViewRaw != null) {
                final Action action = entry.getValue();
                action.setProvider(this);
                action.onRender(actionViewRaw, card);
            }
        }



        final TextView leaveid = findViewById(view, R.id.leaveid, TextView.class);
        if (leaveid != null) {
            leaveid.setText(getMleaveid());
            leaveid.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }
        final TextView leaveidhldr = findViewById(view, R.id.leaveidhldr, TextView.class);
        if (leaveidhldr != null) {
            leaveidhldr.setText(getMleaveidhldr());
            leaveidhldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }


        final TextView leaveappdate = findViewById(view, R.id.leaveappdate, TextView.class);
        if (leaveappdate != null) {
            leaveappdate.setText(getMleaveappdate());
            leaveappdate.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }
        final TextView leaveappdatehldr = findViewById(view, R.id.leaveappdatehldr, TextView.class);
        if (leaveappdatehldr != null) {
            leaveappdatehldr.setText(getMleaveappdatehldr());
            leaveappdatehldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavereason = findViewById(view, R.id.leavereason, TextView.class);
        if (leavereason != null) {
            leavereason.setText(getMleavereason());
            leavereason.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }
        final TextView leavereasonhldr = findViewById(view, R.id.leavereasonhldr, TextView.class);
        if (leavereasonhldr != null) {
            leavereasonhldr.setText(getMleavereasonhldr());
            leavereasonhldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavestart = findViewById(view, R.id.leavestart, TextView.class);
        if (leavestart != null) {
            leavestart.setText(getMleavestart());
            leavestart.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavestarthldr = findViewById(view, R.id.leavestarthldr, TextView.class);
        if (leavestarthldr != null) {
            leavestarthldr.setText(getMleavestarthldr());
            leavestarthldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leaveend = findViewById(view, R.id.leaveend, TextView.class);
        if (leaveend != null) {
            leaveend.setText(getMleaveend());
            leaveend.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leaveendhldr = findViewById(view, R.id.leaveendhldr, TextView.class);
        if (leaveendhldr != null) {
            leaveendhldr.setText(getMleaveendhldr());
            leaveendhldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavetype = findViewById(view, R.id.leavetype, TextView.class);
        if (leavetype != null) {
            leavetype.setText(getMleavetype());
            leavetype.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavetypehldr = findViewById(view, R.id.leavetypehldr, TextView.class);
        if (leavetypehldr != null) {
            leavetypehldr.setText(getMleavetypehldr());
            leavetypehldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavetotaldays = findViewById(view, R.id.leavedays, TextView.class);
        if (leavetotaldays != null) {
            leavetotaldays.setText(getMleavetotaldays());
            leavetotaldays.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavetotaldayshldr = findViewById(view, R.id.leavedayshldr, TextView.class);
        if (leavetotaldayshldr != null) {
            leavetotaldayshldr.setText(getMleavetotaldayshldr());
            leavetotaldayshldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavestatus = findViewById(view, R.id.leavestatus, TextView.class);
        if (leavestatus != null) {
            leavestatus.setText(getMleavestatus());
            leavestatus.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView leavestatushldr = findViewById(view, R.id.leavestatushldr, TextView.class);
        if (leavestatushldr != null) {
            leavestatushldr.setText(getMleavestatushldr());
            leavestatushldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView chatname = findViewById(view, R.id.chatname, TextView.class);
        if (chatname != null) {
            chatname.setText(getChatname());
            chatname.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView chatnamehldr = findViewById(view, R.id.chatnamehldr, TextView.class);
        if (chatnamehldr != null) {
            chatnamehldr.setText(getChatnamehldr());
            chatnamehldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView chatid = findViewById(view, R.id.chatid, TextView.class);
        if (chatid != null) {
            chatid.setText(getChatid());
            chatid.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView chatidhldr = findViewById(view, R.id.chatidhldr, TextView.class);
        if (chatidhldr != null) {
            chatidhldr.setText(getChatidhldr());
            chatidhldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compname = findViewById(view, R.id.compname, TextView.class);
        if (compname != null) {
            compname.setText(getCompname());
            compname.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compnamehldr = findViewById(view, R.id.compnamehldr, TextView.class);
        if (compnamehldr != null) {
            compnamehldr.setText(getCompnamehldr());
            compnamehldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compdesc = findViewById(view, R.id.compdesc, TextView.class);
        if (compdesc != null) {
            compdesc.setText(getCompdesc());
            compdesc.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compdeschldr = findViewById(view, R.id.compdeschldr, TextView.class);
        if (compdeschldr != null) {
            compdeschldr.setText(getCompdeschldr());
            compdeschldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compkey = findViewById(view, R.id.compkey, TextView.class);
        if (compkey != null) {
            compkey.setText(getCompkey());
            compkey.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }

        final TextView compkeyhldr = findViewById(view, R.id.compkeyhldr, TextView.class);
        if (compkeyhldr != null) {
            compkeyhldr.setText(getCompkeyhldr());
            compkeyhldr.setTextColor(view.getResources().getColor(R.color.colorISTSText));
        }



    }

    @Nullable
    protected <V extends View> V findViewById(@NonNull final View view, @IdRes final int id,
                                              @NonNull final Class<V> type) {
        final View viewById = view.findViewById(id);
        if (viewById != null) {
            return type.cast(viewById);
        } else {
            return null;
        }
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Interfaces.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * The OnImageConfigListener will be called, if an image is loaded from an url to an ImageView.
     */
    public interface OnImageConfigListener {
        /**
         * An image is loaded from an url and can be customized now.
         *
         * @param requestCreator to customize the image.
         */
        void onImageConfigure(@NonNull final RequestCreator requestCreator);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Helper methods.
    //
    /////////////////////////////////////////////////////////////////

    /**
     *
     * @param dp
     * @return
     */
    protected int dpToPx(final int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return (int) Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
