package com.dexafree.materialList;

/**
 * Created by Cyrus on 9/13/2016.
 */
public class Book {
    String id;
    String status;
    String author;
    String overview;
    String imageurl;
    String title;
    String rating;
    String bookedition;
    String bookepublished;
    String bookcount;

    public Book(){

    }
    public Book(String id, String title, String author, String status,String imageurl){
        this.id = id;
        this.title = title;
        this.author = author;
        this.status = status;
        this.imageurl = imageurl;
    }
    public Book(String overview, String imageurl, String author, String title, String rating, String bookedition, String bookepublished, String bookcount){
        this.overview = removeHTML(overview);
        this.imageurl = imageurl;
        this.author = author;
        this.title = title;
        this.rating = rating;
        this.bookedition = bookedition;
        this.bookepublished = bookepublished;
        this.bookcount = bookcount;
    }
    public Book(String status){
        this.status = status;
    }

    public String getId(){
        return id;
    }

    public String getStatus(){
        return status;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getEdition() {return bookedition;}

    public void setEdition(String bookedition) {this.bookedition = bookedition;}

    public String getPublish() {return bookepublished;}

    public void setPublish(String bookepublished) {this.bookepublished = bookepublished;}

    public String getCount() {return bookcount;}

    public void setCount(String bookcount) {this.bookcount = bookcount;}

    public static String removeHTML(String htmlString)
    {
        // Remove HTML tag from java String
        String noHTMLString = htmlString.replaceAll("<.*?>", "");
        return noHTMLString;
    }


}
